//
//  JASignupNavigationController.h
//  justask
//
//  Created by Norm Barnard on 8/17/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JASignupNavigationController : UINavigationController

@end
