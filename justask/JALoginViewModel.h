//
//  JALoginViewModel.h
//  justask
//
//  Created by Norm Barnard on 9/3/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "RVMViewModel.h"

@interface JALoginViewModel : RVMViewModel

@property (copy, nonatomic) NSString *email;
@property (copy, nonatomic) NSString *password;
@property (strong, nonatomic, readonly) RACCommand *loginCommand;


@end
