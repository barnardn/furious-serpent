//
//  JAIndicatorTextField.h
//  justask
//
//  Created by Norm Barnard on 2/22/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JAStandardTextField.h"

@class RACCommand;

@interface JAIndicatorTextField : JAStandardTextField

@property (assign, nonatomic, getter = isCorrect) BOOL correct;
@property (nonatomic, readonly) RACCommand *wrongActionCommand;
@end
