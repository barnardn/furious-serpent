//
//  JASignupTableViewCell.h
//  justask
//
//  Created by Norm Barnard on 8/10/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JASignupTableViewCell : UITableViewCell

@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *detail;

+ (CGFloat)rowHeight;

@end
