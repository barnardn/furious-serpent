#import "_Account.h"

@interface Account : _Account {}

+ (instancetype)currentAccount;
+ (instancetype)accountInContext:(NSManagedObjectContext *)context;
+ (instancetype)createNewAccountInContext:(NSManagedObjectContext *)context;
+ (instancetype)createAccountFromJson:(NSDictionary *)json context:(NSManagedObjectContext *)context;

@end
