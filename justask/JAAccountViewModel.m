//
//  JAAccountViewModel.m
//  justask
//
//  Created by Norm Barnard on 8/10/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JustAsk.h"
#import "JADataModel.h"
#import "JAAccountViewModel.h"
#import "JAProfileViewModel.h"

@interface JAAccountViewModel()

@property (strong, nonatomic, readwrite) JAProfileViewModel *profileViewModel;

@end

@implementation JAAccountViewModel


- (instancetype)initWithAccount:(Account *)account
{
    self = [super init];
    if (!self) return nil;
    
    _account = account;
    
    RACChannelTo(self, facebookAccount) = RACChannelTo(self.account, facebookAccountValue);
    
    RAC(self, profileViewModel) = [RACObserve(self.account, profile) map:^id(Profile *profile) {
        return (profile) ? [[JAProfileViewModel alloc] initWithProfile:profile] : [[JAProfileViewModel alloc] initWithAccount:account];
    }];
    
    RACChannelTo(self, firstName) = RACChannelTo(self, profileViewModel.firstName);
    RACChannelTo(self, lastName) = RACChannelTo(self, profileViewModel.lastName);
    RACChannelTo(self, nickName) = RACChannelTo(self, profileViewModel.nickName);
    
    RACChannelTo(self, email) = RACChannelTo(self, email);
    RACChannelTo(self, password) = RACChannelTo(self.account, password);
    
    RAC(self, passwordHash) = [RACObserve(self.account, password) map:^id(NSString *password) {
        return [password ME_SHA1String];
    }];
    
    return self;
}

- (RACSignal *)placeholderForValueKeyPath:(NSString *)valueKeyPath;
{
    NSDictionary *placeholders = @{
        @keypath(self, firstName) : NSLocalizedString(@"William", @"first name placeholder value"),
        @keypath(self, lastName) : NSLocalizedString(@"Shakespeare", @"last name placeholder value"),
        @keypath(self, nickName) : NSLocalizedString(@"Bill", @"nickname placeholder value"),
        @keypath(self, email) : NSLocalizedString(@"wshakespeare@globetheatre.org", @"email placeholder value")
    };
    NSString *placeholder = (placeholders[valueKeyPath]) ?: @"";
    return [RACSignal return:placeholder];
}


- (RACSignal *)createAccountSignal;
{
    @weakify(self);
    // uuid signal will be call to the network
    return [[RACSignal return:[NSString ME_UUIDString]] flattenMap:^RACStream *(NSString *accountIdentity) {
        @strongify(self);
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            self.account.identity = accountIdentity;
            NSError *error;
            BOOL ok = [self.account.managedObjectContext ME_saveRecursively:&error];
            if (!ok) {
                [subscriber sendError:error];
            } else {
                [subscriber sendNext:self];
                [subscriber sendCompleted];
            }
            return nil;
        }];
    }];
}

- (RACSignal *)loginSignal;
{
    return nil;
}

- (RACSignal *)facebookLoginSignal;
{
    return nil;
}

- (RACSignal *)logoutSignal;
{
    return nil;
}

@end
