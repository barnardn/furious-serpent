// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Account.h instead.

#import <CoreData/CoreData.h>

extern const struct AccountAttributes {
	__unsafe_unretained NSString *createdAt;
	__unsafe_unretained NSString *email;
	__unsafe_unretained NSString *facebookAccount;
	__unsafe_unretained NSString *identity;
	__unsafe_unretained NSString *password;
} AccountAttributes;

extern const struct AccountRelationships {
	__unsafe_unretained NSString *profile;
} AccountRelationships;

@class Profile;

@interface AccountID : NSManagedObjectID {}
@end

@interface _Account : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AccountID* objectID;

@property (nonatomic, strong) NSDate* createdAt;

//- (BOOL)validateCreatedAt:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* email;

//- (BOOL)validateEmail:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* facebookAccount;

@property (atomic) BOOL facebookAccountValue;
- (BOOL)facebookAccountValue;
- (void)setFacebookAccountValue:(BOOL)value_;

//- (BOOL)validateFacebookAccount:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* identity;

//- (BOOL)validateIdentity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* password;

//- (BOOL)validatePassword:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Profile *profile;

//- (BOOL)validateProfile:(id*)value_ error:(NSError**)error_;

@end

@interface _Account (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveCreatedAt;
- (void)setPrimitiveCreatedAt:(NSDate*)value;

- (NSString*)primitiveEmail;
- (void)setPrimitiveEmail:(NSString*)value;

- (NSNumber*)primitiveFacebookAccount;
- (void)setPrimitiveFacebookAccount:(NSNumber*)value;

- (BOOL)primitiveFacebookAccountValue;
- (void)setPrimitiveFacebookAccountValue:(BOOL)value_;

- (NSString*)primitiveIdentity;
- (void)setPrimitiveIdentity:(NSString*)value;

- (NSString*)primitivePassword;
- (void)setPrimitivePassword:(NSString*)value;

- (Profile*)primitiveProfile;
- (void)setPrimitiveProfile:(Profile*)value;

@end
