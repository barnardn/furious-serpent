//
//  JALegalTermsNavigationController.h
//  justask
//
//  Created by Norm Barnard on 9/2/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JALegalTermsNavigationController : UINavigationController

@end
