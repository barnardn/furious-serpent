//
//  JATextFieldTableViewCell.h
//  justask
//
//  Created by Norm Barnard on 9/1/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JATextFieldTableViewCell : UITableViewCell

@property (strong, nonatomic) id viewModel;
@property (copy, nonatomic) NSString *name;
@property (copy,nonatomic) NSString *valueKeyPath;
@property (copy, nonatomic) NSString *placeholderValue;
@property (assign, nonatomic) CGFloat nameLabelAlignmentWidth;


@end
