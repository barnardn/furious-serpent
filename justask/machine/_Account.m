// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Account.m instead.

#import "_Account.h"

const struct AccountAttributes AccountAttributes = {
	.createdAt = @"createdAt",
	.email = @"email",
	.facebookAccount = @"facebookAccount",
	.identity = @"identity",
	.password = @"password",
};

const struct AccountRelationships AccountRelationships = {
	.profile = @"profile",
};

@implementation AccountID
@end

@implementation _Account

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Account" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Account";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Account" inManagedObjectContext:moc_];
}

- (AccountID*)objectID {
	return (AccountID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"facebookAccountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"facebookAccount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic createdAt;

@dynamic email;

@dynamic facebookAccount;

- (BOOL)facebookAccountValue {
	NSNumber *result = [self facebookAccount];
	return [result boolValue];
}

- (void)setFacebookAccountValue:(BOOL)value_ {
	[self setFacebookAccount:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveFacebookAccountValue {
	NSNumber *result = [self primitiveFacebookAccount];
	return [result boolValue];
}

- (void)setPrimitiveFacebookAccountValue:(BOOL)value_ {
	[self setPrimitiveFacebookAccount:[NSNumber numberWithBool:value_]];
}

@dynamic identity;

@dynamic password;

@dynamic profile;

@end

