//
//  JABaseViewController.m
//  justask
//
//  Created by Norm Barnard on 2/14/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <MEReactiveFoundation/MEReactiveFoundation.h>
#import <SVProgressHUD/SVProgressHUD.h>

#import "JABaseViewController.h"

@interface JABaseViewController ()<UIAlertViewDelegate>

@property (copy, nonatomic) void (^alertCompletion)();

@end

@implementation JABaseViewController

- (id)init
{
    self = [super init];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - progress hud methods

- (void)showProgressHudWithMessage:(NSString *)message
{
    if ([message length] == 0)
        [SVProgressHUD show];
    else
        [SVProgressHUD showWithStatus:message];
}

- (void)dismissProgressHud
{
    [SVProgressHUD dismiss];
}


- (id)findFirstResponder;
{
    NSArray *results = [self.view.subviews MER_filter:^BOOL(id value) {
        return (([value isKindOfClass:[UIResponder class]]) && ([value isFirstResponder]));
    }];
    return [results firstObject];
}



@end
