//
//  JASettingsNavigationController.h
//  justask
//
//  Created by Norm Barnard on 12/26/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JASettingsNavigationController : UINavigationController

@end
