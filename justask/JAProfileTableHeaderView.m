//
//  JAProfileTableHeaderView.m
//  justask
//
//  Created by Norm Barnard on 11/12/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JustAsk.h"
#import "JAProfileTableHeaderView.h"
#import "JAProfileTableHeaderContentView.h"

@interface JAProfileTableHeaderView()

@property (weak, nonatomic) JAProfileTableHeaderContentView *titleHeaderView;

@end

@implementation JAProfileTableHeaderView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    JAProfileTableHeaderContentView *headerView = [[JAProfileTableHeaderContentView alloc] initWithFrame:CGRectZero];
    [self.contentView addSubview:headerView];
    self.titleHeaderView = headerView;
    
    RACChannelTo(self.titleHeaderView, title) = RACChannelTo(self, title);
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.titleHeaderView.frame = self.contentView.bounds;
}

+ (CGFloat)rowHeight {
    return 24.0f;
}

@end
