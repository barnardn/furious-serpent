//
//  UIButton+JustAsk.h
//  justask
//
//  Created by Norm Barnard on 2/16/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (JustAsk)

+ (UIButton *)JA_ButtonWithTitle:(NSString *)title frame:(CGRect)frame;
+ (UIButton *)JA_ButtonWithImage:(UIImage *)image frame:(CGRect)frame;

@end
