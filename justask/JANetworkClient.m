//
//  JANetworkClient.m
//  justask
//
//  Created by Norm Barnard on 11/2/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "FTHTTPCodes.h"
#import "JustAsk.h"
#import "JAAppController.h"
#import "JANetworkClient.h"


static JANetworkClient *staticClient;

const struct JustAskApplicationHeaders {
    __unsafe_unretained NSString *apiHeader;
    __unsafe_unretained NSString *authorizationHeader;
} JustAskApplicationHeaders = {
    .apiHeader = @"X-Justask-Api-Token",
    .authorizationHeader = @"Authorization"
};


@interface JANetworkClient ()

@property (strong, nonatomic) AFURLSessionManager *downloadSessionManager;

@end

@implementation JANetworkClient

- (instancetype)init
{
    if (staticClient) return staticClient;
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    [sessionConfig setHTTPAdditionalHeaders:@{ JustAskApplicationHeaders.apiHeader : [JAAppController appController].apiKey}];
    
    self = [super initWithBaseURL:[JAAppController appController].hostAddress sessionConfiguration:sessionConfig];
    if (!self) return nil;
    
    self.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:0];
    [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    self.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];

    _downloadSessionManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    return self;
}

#pragma mark - property overrides

- (void)setSessionToken:(NSString *)sessionToken
{
    @synchronized(self) {
        _sessionToken = sessionToken;
        [self.requestSerializer setValue:sessionToken forHTTPHeaderField:JustAskApplicationHeaders.authorizationHeader];
    }
}

#pragma mark - api signals

- (RACSignal *)getRequestAtPath:(NSString *)path parameters:(NSDictionary *)parameters;
{
    @weakify(self);
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {

        NSURLSessionDataTask *task = [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
            [subscriber sendNext:responseObject];
            [subscriber sendCompleted];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            @strongify(self);
            NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
            [subscriber sendError:[self _applicationErrorWithResponse:response error:error]];
        }];
        
        return [RACDisposable disposableWithBlock:^{
            [task cancel];
        }];
        
    }];
}

- (RACSignal *)postRequestAtPath:(NSString *)path parameters:(NSDictionary *)parameters;
{
    @weakify(self);
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        NSURLSessionDataTask *task = [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
            [subscriber sendNext:responseObject];
            [subscriber sendCompleted];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            @strongify(self);
            NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
            [subscriber sendError:[self _applicationErrorWithResponse:response error:error]];
        }];

        return [RACDisposable disposableWithBlock:^{
            [task cancel];
        }];
        
    }];
}

- (RACSignal *)putRequestAtPath:(NSString *)path parameters:(NSDictionary *)parameters;
{
    @weakify(self);
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        NSURLSessionDataTask *task = [self PUT:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
            [subscriber sendNext:responseObject];
            [subscriber sendCompleted];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            @strongify(self);
            NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
            [subscriber sendError:[self _applicationErrorWithResponse:response error:error]];
        }];
        
        return [RACDisposable disposableWithBlock:^{
            [task cancel];
        }];
        
    }];
}

- (RACSignal *)deleteRequestAtPath:(NSString *)path parameters:(NSDictionary *)parameters;
{
    @weakify(self);
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        NSURLSessionDataTask *task = [self DELETE:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
            [subscriber sendNext:responseObject];
            [subscriber sendCompleted];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            @strongify(self);
            NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
            [subscriber sendError:[self _applicationErrorWithResponse:response error:error]];
        }];
        
        return [RACDisposable disposableWithBlock:^{
            [task cancel];
        }];
        
    }];
}

- (RACSignal *)downloadResourceFromURL:(NSURL *)remoteURL toLocalURL:(NSURL *)localURL headers:(NSDictionary *)headers {
    @weakify(self);
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:remoteURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0f];
        [request setAllHTTPHeaderFields:headers];

        NSProgress *downloadProgress;
        NSURLSessionDownloadTask *task = [self.downloadSessionManager downloadTaskWithRequest:request progress:&downloadProgress destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
            
            return localURL;
            
        } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {

            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            if (error) {
                [subscriber sendError:[self _applicationErrorWithResponse:httpResponse error:error]];
            } else {
                [subscriber sendNext:RACTuplePack(localURL, @100, @(response.expectedContentLength))];
                [subscriber sendCompleted];
            }
        }];
        
        [RACObserve(downloadProgress, fractionCompleted) subscribeNext:^(NSNumber *value) {
            MELog(@"progress: %@ of %@", @(value.doubleValue * downloadProgress.totalUnitCount), @(downloadProgress.totalUnitCount));
            [subscriber sendNext:RACTuplePack(localURL, value, @(downloadProgress.totalUnitCount))];
        }];
        
        [task resume];
        
        return [RACDisposable disposableWithBlock:^{
            [task cancel];
        }];
    }];
}



- (NSError *)_applicationErrorWithResponse:(NSHTTPURLResponse *)response error:(NSError *)error
{
    NSString *applicationMessage = [NSHTTPURLResponse localizedStringForStatusCode:response.statusCode];
    
    switch (response.statusCode) {
        case HTTPCode401Unauthorised:
            applicationMessage = NSLocalizedString(@"Your username or password is not correct. Check your spelling and try again", @"unauthorized server message");
            break;
        case HTTPCode403Forbidden:
            applicationMessage = NSLocalizedString(@"You are not permitted to use this application at this time.", @"403 server message");
            break;
        default:
            break;
    }
    NSDictionary *userInfo =
    @{
       NSLocalizedDescriptionKey : applicationMessage,
       JustAskApplicationNetworkErrorKey : (error) ?: [NSNull null]
    };
    
    return [NSError errorWithDomain:JustAskApplicationErrorDomain code:response.statusCode userInfo:userInfo];
}

+ (instancetype)client
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        staticClient = [[[self class] alloc] init];
    });
    return staticClient;
}

@end
