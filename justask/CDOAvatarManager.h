//
//  CDOAvatarManager.h
//  justask
//
//  Created by Norm Barnard on 12/21/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDOAvatarManager : NSObject

@property (copy, nonatomic) NSString *applicationPath;        // custom path used by avatars.io 

- (instancetype)initWithUploadToken:(NSString *)token;
- (void)uploadAvatarAtURL:(NSURL *)fileURL success:(void(^)(NSURL *avatarURL))successBlock failure:(void(^)(NSURLResponse *response, NSError *error))failureBlock;


@end
