//
//  JAPersonalDetailsCell.m
//  justask
//
//  Created by Norm Barnard on 11/10/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JustAsk.h"
#import "JAAppearanceManager.h"
#import "JAPersonalDetailsCell.h"
#import "JAProfileViewModel.h"

@interface JAPersonalDetailsCell() <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *profilePictureButton;
@property (weak, nonatomic) IBOutlet UITextField *firstNameField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameField;
@property (weak, nonatomic) IBOutlet UITextField *nickNameField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *avatarSaveActivityIndicator;

@end


@implementation JAPersonalDetailsCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.avatarSaveActivityIndicator.tintColor = [JAAppearanceManager appearanceManager].brandColor;
    
    RAC(self, viewModel.firstName) = self.firstNameField.rac_textSignal;
    RAC(self, viewModel.lastName) = self.lastNameField.rac_textSignal;
    RAC(self, viewModel.nickName) = self.nickNameField.rac_textSignal;
    
    RAC(self.profilePictureButton, rac_command) = [RACObserve(self, viewModel) map:^id(JAProfileViewModel *vm) {
        return vm.profileImageCommand;
    }];

    @weakify(self);
    
    [[[[RACObserve(self, viewModel.storingAvatar) ignore:nil] distinctUntilChanged]
      
      deliverOn:[RACScheduler mainThreadScheduler]]
     
     subscribeNext:^(NSNumber *value) {
         
         @strongify(self);
         self.profilePictureButton.alpha = (value.boolValue) ? 0.33f : 1.0f;
         if (value.boolValue)
             [self.avatarSaveActivityIndicator startAnimating];
         else
             [self.avatarSaveActivityIndicator stopAnimating];
         self.profilePictureButton.enabled = !value.boolValue;
         
     }];
    
    [[[[RACObserve(self,viewModel) distinctUntilChanged] flattenMap:^RACStream *(JAProfileViewModel *vm) {
        return [vm.profileImage map:^id(RACTuple *tuple) {
            NSURL *localURL = tuple.first;
            return [UIImage imageWithData:[NSData dataWithContentsOfURL:localURL] scale:[UIScreen mainScreen].scale];
        }];
    }] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(UIImage *image) {
        @strongify(self);
         self.profilePictureButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.profilePictureButton setImage:image forState:UIControlStateNormal];
    }];
    
    self.profilePictureButton.layer.borderWidth = 1.0f;
    self.profilePictureButton.layer.borderColor = [JAAppearanceManager appearanceManager].brandColor.CGColor;
    self.profilePictureButton.layer.cornerRadius = 3.0f;
    self.profilePictureButton.clipsToBounds = YES;

    self.firstNameField.placeholder = NSLocalizedString(@"First name", @"profile cell first name placeholder");
    self.lastNameField.placeholder = NSLocalizedString(@"Last name", @"profiel cell last name placeholder");
    self.nickNameField.placeholder = NSLocalizedString(@"Nick Name (optional)", @"profile cell nickname placeholder");
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.firstNameField)
        [self.lastNameField becomeFirstResponder];
    else if (textField == self.lastNameField)
        [self.nickNameField becomeFirstResponder];
    else
        [self endEditing:YES];
    return NO;
}


+ (CGFloat)rowHeight {
    return 125.0f;
}

@end
