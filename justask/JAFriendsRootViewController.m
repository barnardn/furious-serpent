//
//  JAFriendsRootViewController.m
//  justask
//
//  Created by Norm Barnard on 2/24/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JAFriendsRootViewController.h"

@interface JAFriendsRootViewController ()

@end

@implementation JAFriendsRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (UITabBarItem *)tabBarItem
{
    UITabBarItem *tbi = [super tabBarItem];
    [tbi setTitle:NSLocalizedString(@"Friends", @"friend view tab bar title")];
    [tbi setImage:[[UIImage imageNamed:@"icon-tabbar-friend"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    return tbi;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

@end
