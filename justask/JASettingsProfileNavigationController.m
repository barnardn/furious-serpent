//
//  JASettingsProfileNavigationController.m
//  justask
//
//  Created by Norm Barnard on 11/12/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JASettingsProfileNavigationController.h"
#import "JASettingsProfileViewController.h"

@interface JASettingsProfileNavigationController ()

@end

@implementation JASettingsProfileNavigationController

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.viewControllers = @[[[JASettingsProfileViewController alloc] init]];
    
    return self;
}



@end
