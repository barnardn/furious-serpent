//
//  JAAccountViewModel.h
//  justask
//
//  Created by Norm Barnard on 8/10/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "RVMViewModel.h"

@class Account;
@class JAProfileViewModel;

@interface JAAccountViewModel : RVMViewModel

- (instancetype)initWithAccount:(Account *)account;


// voter properties - shared with other users
@property (copy, nonatomic) NSString *firstName;
@property (copy, nonatomic) NSString *lastName;
@property (copy, nonatomic) NSString *nickName;
@property (copy, nonatomic) NSString *email;

@property (strong, nonatomic) NSArray *passwordStrengthViolations;      // rules broken for invalid password

// account properties - specific to a user and not shared

@property (assign, nonatomic) BOOL acceptPropositionsFromFriendsOnly;
@property (assign, nonatomic) BOOL facebookAccount;
@property (strong, nonatomic) NSString *password;

@property (copy, nonatomic) NSString *passwordHash;

@property (strong, nonatomic, readonly) Account *account;
@property (strong, nonatomic, readonly) JAProfileViewModel *profileViewModel;

- (RACSignal *)placeholderForValueKeyPath:(NSString *)valueKeyPath;

- (RACSignal *)createAccountSignal;
- (RACSignal *)loginSignal;
- (RACSignal *)facebookLoginSignal;
- (RACSignal *)logoutSignal;


@end
