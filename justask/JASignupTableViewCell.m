//
//  JASignupTableViewCell.m
//  justask
//
//  Created by Norm Barnard on 8/10/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JustAsk.h"
#import "JAAppearanceManager.h"
#import "JASignupTableViewCell.h"

@interface JASignupTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@end


@implementation JASignupTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.nameLabel.font = [[JAAppearanceManager appearanceManager] cellLabelFont];
    self.detailLabel.font = [[JAAppearanceManager appearanceManager] cellDetailsFont];
    
    self.nameLabel.textColor = [[JAAppearanceManager appearanceManager] brandColor];
    self.detailLabel.textColor = [[JAAppearanceManager appearanceManager] captionTextColor];
    
    RACChannelTo(self.nameLabel, text) = RACChannelTo(self, name);
    RACChannelTo(self.detailLabel, text) = RACChannelTo(self, detail);
}

+ (CGFloat)rowHeight; {
    return 44.0f;
}

@end
