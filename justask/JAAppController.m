//
//  JAAppController.m
//  justask
//
//  Created by Norm Barnard on 2/8/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <MECoreDataKit/MECoreDataKit.h>
#import <FacebookSDK/FacebookSDK.h>

#import "JustAsk.h"
#import "JAAppController.h"
#import "JAAppearanceManager.h"

NSString * const JustAskApplicationErrorDomain = @"com.justask.app-error";
NSString * const JustAskApplicationNetworkErrorKey = @"JustAskApplicationNetworkErrorKey";

static NSString * const kDataModelName = @"justask";
static NSString * const kCoreDataStoreName = @"justask.sqlite";
static NSString * const kFacebookApplicationId = @"1394672670794671";    // facebook : furiousserpent

static const struct JustAskAppConfigOptions {
    __unsafe_unretained NSString *hostAddressKey;
    __unsafe_unretained NSString *apiKey;
    __unsafe_unretained NSString *avatarsIOUploadToken;
} JustAskConfigOptions = {
    .hostAddressKey = @"JAServerHostname",
    .apiKey = @"JAAPIKey",
    .avatarsIOUploadToken = @"JAAvatarsIOUploadToken"
};


@interface JAAppController()

@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) NSManagedObjectContext *writeManagedObjectContext;
@property (strong, nonatomic, readwrite) NSManagedObjectContext *mainManagedObjectContext;

@end


@implementation JAAppController


- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    [self _configureCoreData];
    [[JAAppearanceManager appearanceManager] setupAppearanceProxies];
    return self;
}


#pragma mark - private methods

- (void)_configureCoreData
{
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:kDataModelName withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:_managedObjectModel];
    
    NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption : @true, NSInferMappingModelAutomaticallyOption : @true};
#ifdef DEBUG
    options = @{NSMigratePersistentStoresAutomaticallyOption : @true, NSInferMappingModelAutomaticallyOption : @true, NSSQLitePragmasOption : @{ @"journal_mode" : @"DELETE" }};
#endif
    NSURL *storeURL = [self urlForResourceInApplicationSupport:kCoreDataStoreName];
#ifdef NUKE_DATABASE
    [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
#endif
    NSError *error;
    [_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error];
    MEAssert(error == nil, @"Unable to add persistent store: %@", error);
    
    _writeManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_writeManagedObjectContext setUndoManager:nil];
    [_writeManagedObjectContext setPersistentStoreCoordinator:_persistentStoreCoordinator];
    
    _mainManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_mainManagedObjectContext setUndoManager:nil];
    [_mainManagedObjectContext setPersistentStoreCoordinator:_persistentStoreCoordinator];
    
    [NSManagedObjectContext ME_setDefaultIdentityKey:@"identity"];
    
    MELogObject(storeURL);
    
}

#pragma mark - configuration properties

@dynamic hostAddress;

- (NSURL *)hostAddress
{
    NSString *host = [[NSProcessInfo processInfo] environment][@"HOSTURL"];
    if (host.length > 0) {
        return [NSURL URLWithString:host];
    }
    NSString *address = [[NSBundle mainBundle] objectForInfoDictionaryKey:JustAskConfigOptions.hostAddressKey];
    return [NSURL URLWithString:address];
}

@dynamic apiKey;

- (NSString *)apiKey {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:JustAskConfigOptions.apiKey];
}

@dynamic avatarUploadToken;

- (NSString *)avatarUploadToken {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:JustAskConfigOptions.avatarsIOUploadToken];
}

#pragma mark - local filesystem path methods

- (NSURL *)urlForResourceInApplicationSupport:(NSString *)resourceName
{
    if (!resourceName) return nil;
    NSError *error;
    NSURL *appSupport = [[NSFileManager defaultManager] URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:&error];
    MEAssert(error == nil, @"Unable to create directory in application support: %@", error);
    NSURL *resourceURL = [NSURL fileURLWithPathComponents:@[ [appSupport path], resourceName]];
    return resourceURL;
}

- (NSURL *)urlForResourceInCache:(NSString *)resourceName
{
    NSURL *cacheDir = [[[NSFileManager defaultManager] URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] lastObject];
    return [cacheDir URLByAppendingPathComponent:resourceName];
}


- (NSURL *)urlForResourceNamed:(NSString *)resourceName inFolderNamed:(NSString *)folderName;
{
    NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] firstObject];
    NSURL *folderURL = [url URLByAppendingPathComponent:folderName];
    if (![folderURL checkResourceIsReachableAndReturnError:nil])
        if (![self _createFolderAtURL:folderURL]) return nil;
    return [folderURL URLByAppendingPathComponent:resourceName];
}

- (NSURL *)urlForResourceInMainBundle:(NSString *)resourceName;
{
    return [[NSBundle mainBundle] URLForResource:resourceName withExtension:nil];
}

- (BOOL)_createFolderAtURL:(NSURL *)folderURL
{
    NSError *error;
    BOOL ok = [[NSFileManager defaultManager] createDirectoryAtURL:folderURL withIntermediateDirectories:YES attributes:nil error:&error];
    MEAssert(error == nil, @"Unable to create folder %@", folderURL);
    return ok;
}

#pragma mark - class methods

+ (instancetype)appController
{
    static JAAppController *staticInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        staticInstance = [[[self class] alloc] init];
    });
    return staticInstance;
}

@end
