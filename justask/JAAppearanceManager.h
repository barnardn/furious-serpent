//
//  JAAppearanceManager.h
//  justask
//
//  Created by Norm Barnard on 2/14/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JAAppearanceManager : NSObject

- (void)setupAppearanceProxies;

- (UIColor *)brandColor;
- (UIColor *)whiteColor;            // this app's definition of white
- (UIColor *)blackColor;            // this app's definition of black
- (UIColor *)disabledColor;

- (UIColor *)buttonTintColor;
- (UIColor *)buttonTitleColor;
- (UIColor *)bodyTextColor;         // for user input and large blocks of text
- (UIColor *)cellHeaderFooterTitleColor;
- (UIColor *)groupedTableViewBackgroundColor;

- (UIColor *)captionTextColor;      // for labels of text no longer than one line - typically informative text
- (UIColor *)controlBorderColor;    // for borders around text[view|field] and cell separators

- (UIFont *)appTitleFont;           // for use in all labels where the Just Ask. app name is displayed
- (UIFont *)titleFont;              // for use in buttons and single line labels
- (UIFont *)subTitleFont;           // for use in subtitles for views and cells to augment a main title
- (UIFont *)cellHeaderFooterFont;    // tableview header/footer label font.
- (UIFont *)userInputFont;          // for use in textfield and text views.

- (UIFont *)captionTextFont;        // for informative short labels
- (UIFont *)smallCaptionTextFont;

- (UIFont *)cellLabelFont;          // in label,value cells, the label text
- (UIFont *)cellDetailsFont;        // in label,value cells, the value text

+ (instancetype)appearanceManager;

@end
