//
//  JAEditVoteTopicViewController.m
//  justask
//
//  Created by Norm Barnard on 7/14/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JAEditVoteTopicViewController.h"

@interface JAEditVoteTopicViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) id viewModel;

@end

@implementation JAEditVoteTopicViewController

- (id)initWithViewModel:(id)viewModel
{
    self = [super init];
    if (!self) return nil;
    _viewModel = viewModel;
    return self;
}

- (NSString *)nibName
{
    return @"JAEditVoteTopicView";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:nil action:nil];
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:nil action:nil];
    
    self.navigationItem.leftBarButtonItem = cancelButton;
    self.navigationItem.rightBarButtonItem = saveButton;
    
}

#pragma mark - tableview datasource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}



@end
