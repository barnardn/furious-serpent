// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SecurityQuestion.h instead.

#import <CoreData/CoreData.h>

extern const struct SecurityQuestionAttributes {
	__unsafe_unretained NSString *answer;
	__unsafe_unretained NSString *identity;
	__unsafe_unretained NSString *question;
	__unsafe_unretained NSString *userDefined;
} SecurityQuestionAttributes;

@interface SecurityQuestionID : NSManagedObjectID {}
@end

@interface _SecurityQuestion : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) SecurityQuestionID* objectID;

@property (nonatomic, strong) NSString* answer;

//- (BOOL)validateAnswer:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* identity;

//- (BOOL)validateIdentity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* question;

//- (BOOL)validateQuestion:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* userDefined;

@property (atomic) BOOL userDefinedValue;
- (BOOL)userDefinedValue;
- (void)setUserDefinedValue:(BOOL)value_;

//- (BOOL)validateUserDefined:(id*)value_ error:(NSError**)error_;

@end

@interface _SecurityQuestion (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAnswer;
- (void)setPrimitiveAnswer:(NSString*)value;

- (NSString*)primitiveIdentity;
- (void)setPrimitiveIdentity:(NSString*)value;

- (NSString*)primitiveQuestion;
- (void)setPrimitiveQuestion:(NSString*)value;

- (NSNumber*)primitiveUserDefined;
- (void)setPrimitiveUserDefined:(NSNumber*)value;

- (BOOL)primitiveUserDefinedValue;
- (void)setPrimitiveUserDefinedValue:(BOOL)value_;

@end
