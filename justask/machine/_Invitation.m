// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Invitation.m instead.

#import "_Invitation.h"

const struct InvitationAttributes InvitationAttributes = {
	.createdAt = @"createdAt",
	.finalizedAt = @"finalizedAt",
	.identity = @"identity",
	.status = @"status",
	.summary = @"summary",
};

const struct InvitationRelationships InvitationRelationships = {
	.creator = @"creator",
	.invitee = @"invitee",
};

@implementation InvitationID
@end

@implementation _Invitation

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Invitation" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Invitation";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Invitation" inManagedObjectContext:moc_];
}

- (InvitationID*)objectID {
	return (InvitationID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"statusValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"status"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic createdAt;

@dynamic finalizedAt;

@dynamic identity;

@dynamic status;

- (int16_t)statusValue {
	NSNumber *result = [self status];
	return [result shortValue];
}

- (void)setStatusValue:(int16_t)value_ {
	[self setStatus:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveStatusValue {
	NSNumber *result = [self primitiveStatus];
	return [result shortValue];
}

- (void)setPrimitiveStatusValue:(int16_t)value_ {
	[self setPrimitiveStatus:[NSNumber numberWithShort:value_]];
}

@dynamic summary;

@dynamic creator;

@dynamic invitee;

@end

