//
//  JABaseViewController.h
//  justask
//
//  Created by Norm Barnard on 2/14/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <MEReactiveKit/MERViewController.h>
#import <UIKit/UIKit.h>

@interface JABaseViewController : MERViewController


// * progress / activity indicator methods

- (void)showProgressHudWithMessage:(NSString *)message;
- (void)dismissProgressHud;

// * first responder finder

- (id)findFirstResponder;

@end
