//
//  JALegalTermsViewController.h
//  justask
//
//  Created by Norm Barnard on 9/2/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JALegalTextViewController : UIViewController

- (instancetype)initWithLegalDocumentURL:(NSURL *)documentURL title:(NSString *)title;

@end
