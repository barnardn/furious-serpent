//
//  JASignupViewModel.m
//  justask
//
//  Created by Norm Barnard on 11/2/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <MEFoundation/NSSet+MEExtensions.h>
#import <MECoreDataKit/MECoreDataKit.h>
#import <SVProgressHUD/SVProgressHUD.h>

#import "Account.h"
#import "JustAsk.h"
#import "JAAppController.h"
#import "JASignupViewModel.h"
#import "JAAccountViewModel.h"
#import "JANetworkClient.h"
#import "NSString+InputValidation.h"


static NSString * const kSignupAPIPath = @"/api/v1/account";

@interface JASignupViewModel()

@property (strong, nonatomic, readwrite) RACCommand *signupCommand;

@end

@implementation JASignupViewModel

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    @weakify(self);
    self.signupCommand = [[RACCommand alloc] initWithEnabled:[self _validationSignal] signalBlock:^RACSignal *(id _) {
        
        NSDictionary *parameters = @{ @"email" : self.email,
                                      @"passwordHash" : [self.password ME_SHA1String]
                                    };
        return [[[[[JANetworkClient client]
                   
                postRequestAtPath:kSignupAPIPath parameters:parameters]
                  
                initially:^{
                    
                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Creating Account...", @"signup creating account HUD message")];
                    
                }] doNext:^(NSDictionary *responseJson) {
                    
                    @strongify(self);
                    Account *account = [Account createNewAccountInContext:[JAAppController appController].mainManagedObjectContext];
                    account.email = self.email;
                    account.password = self.password;
                    account.identity = responseJson[AccountAttributes.identity];
                    [[JAAppController appController].mainManagedObjectContext ME_saveRecursively:NULL];
                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Success! Log in to start usign the app.", @"signup account created success HUD message")];
                    
                }] doError:^(NSError *error) {
                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Error.", @"signup account erro HUD message")];
                }];
    }];
    
    return self;
}


- (RACSignal *)_validationSignal
{
    return [RACSignal combineLatest:@[ RACObserve(self, email), RACObserve(self, password) ] reduce:^id(NSString *email, NSString *password) {
        return @([email isValidRFC2822EmailAddress] && [password isValidPasswordOrViolatingRules:NULL]);
    }];
}


@end
