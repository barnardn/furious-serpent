//
//  JAVoteDashboardViewController.m
//  justask
//
//  Created by Norm Barnard on 2/24/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JAVoteDashboardViewController.h"

@interface JAVoteDashboardViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) UITableView *tableView;

@end

@implementation JAVoteDashboardViewController

- (id)init
{
    self = [super init];
    if (!self) return nil;
    return self;
}

- (UITabBarItem *)tabBarItem
{
    UITabBarItem *tbi = [super tabBarItem];
    [tbi setTitle:NSLocalizedString(@"Ask", @"ask view tab bar title")];
    [tbi setImage:[[UIImage imageNamed:@"icon-tabbar-ask"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    return tbi;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Votes", @"ask tab view navigation title");
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:nil action:nil];
    self.navigationItem.rightBarButtonItem = addButton;
}

@end
