//
//  JALegalTermsViewController.m
//  justask
//
//  Created by Norm Barnard on 9/2/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JAAppearanceManager.h"
#import "JAAppController.h"
#import "JALegalTextViewController.h"

@interface JALegalTextViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) NSURL *documentURL;
@end

@implementation JALegalTextViewController

- (instancetype)initWithLegalDocumentURL:(NSURL *)documentURL title:(NSString *)title;
{
    self = [super init];
    if (!self) return nil;
    self.title = title;
    _documentURL = documentURL;
    return self;
}

- (NSString *)nibName
{
    return @"JALegalTextView";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSAttributedString *attributedStringWithRtf = [[NSAttributedString alloc] initWithFileURL:self.documentURL options:@{NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType} documentAttributes:nil error:nil];
    self.textView.attributedText = attributedStringWithRtf;
}

@end
