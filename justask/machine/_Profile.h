// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Profile.h instead.

#import <CoreData/CoreData.h>

extern const struct ProfileAttributes {
	__unsafe_unretained NSString *adhoc;
	__unsafe_unretained NSString *approved;
	__unsafe_unretained NSString *avatarUrl;
	__unsafe_unretained NSString *city;
	__unsafe_unretained NSString *country;
	__unsafe_unretained NSString *createdAt;
	__unsafe_unretained NSString *firstName;
	__unsafe_unretained NSString *identity;
	__unsafe_unretained NSString *lastName;
	__unsafe_unretained NSString *latitude;
	__unsafe_unretained NSString *longitude;
	__unsafe_unretained NSString *nickName;
	__unsafe_unretained NSString *updatedAt;
} ProfileAttributes;

extern const struct ProfileRelationships {
	__unsafe_unretained NSString *account;
	__unsafe_unretained NSString *ballots;
	__unsafe_unretained NSString *receivedInvitations;
	__unsafe_unretained NSString *sentInvitations;
} ProfileRelationships;

@class Account;
@class Ballot;
@class Invitation;
@class Invitation;

@interface ProfileID : NSManagedObjectID {}
@end

@interface _Profile : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ProfileID* objectID;

@property (nonatomic, strong) NSNumber* adhoc;

@property (atomic) BOOL adhocValue;
- (BOOL)adhocValue;
- (void)setAdhocValue:(BOOL)value_;

//- (BOOL)validateAdhoc:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* approved;

@property (atomic) BOOL approvedValue;
- (BOOL)approvedValue;
- (void)setApprovedValue:(BOOL)value_;

//- (BOOL)validateApproved:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* avatarUrl;

//- (BOOL)validateAvatarUrl:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* city;

//- (BOOL)validateCity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* country;

//- (BOOL)validateCountry:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* createdAt;

//- (BOOL)validateCreatedAt:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* firstName;

//- (BOOL)validateFirstName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* identity;

//- (BOOL)validateIdentity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* lastName;

//- (BOOL)validateLastName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDecimalNumber* latitude;

//- (BOOL)validateLatitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDecimalNumber* longitude;

//- (BOOL)validateLongitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* nickName;

//- (BOOL)validateNickName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* updatedAt;

//- (BOOL)validateUpdatedAt:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Account *account;

//- (BOOL)validateAccount:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Ballot *ballots;

//- (BOOL)validateBallots:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Invitation *receivedInvitations;

//- (BOOL)validateReceivedInvitations:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *sentInvitations;

- (NSMutableSet*)sentInvitationsSet;

@end

@interface _Profile (SentInvitationsCoreDataGeneratedAccessors)
- (void)addSentInvitations:(NSSet*)value_;
- (void)removeSentInvitations:(NSSet*)value_;
- (void)addSentInvitationsObject:(Invitation*)value_;
- (void)removeSentInvitationsObject:(Invitation*)value_;

@end

@interface _Profile (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAdhoc;
- (void)setPrimitiveAdhoc:(NSNumber*)value;

- (BOOL)primitiveAdhocValue;
- (void)setPrimitiveAdhocValue:(BOOL)value_;

- (NSNumber*)primitiveApproved;
- (void)setPrimitiveApproved:(NSNumber*)value;

- (BOOL)primitiveApprovedValue;
- (void)setPrimitiveApprovedValue:(BOOL)value_;

- (NSString*)primitiveAvatarUrl;
- (void)setPrimitiveAvatarUrl:(NSString*)value;

- (NSString*)primitiveCity;
- (void)setPrimitiveCity:(NSString*)value;

- (NSString*)primitiveCountry;
- (void)setPrimitiveCountry:(NSString*)value;

- (NSDate*)primitiveCreatedAt;
- (void)setPrimitiveCreatedAt:(NSDate*)value;

- (NSString*)primitiveFirstName;
- (void)setPrimitiveFirstName:(NSString*)value;

- (NSString*)primitiveIdentity;
- (void)setPrimitiveIdentity:(NSString*)value;

- (NSString*)primitiveLastName;
- (void)setPrimitiveLastName:(NSString*)value;

- (NSDecimalNumber*)primitiveLatitude;
- (void)setPrimitiveLatitude:(NSDecimalNumber*)value;

- (NSDecimalNumber*)primitiveLongitude;
- (void)setPrimitiveLongitude:(NSDecimalNumber*)value;

- (NSString*)primitiveNickName;
- (void)setPrimitiveNickName:(NSString*)value;

- (NSDate*)primitiveUpdatedAt;
- (void)setPrimitiveUpdatedAt:(NSDate*)value;

- (Account*)primitiveAccount;
- (void)setPrimitiveAccount:(Account*)value;

- (Ballot*)primitiveBallots;
- (void)setPrimitiveBallots:(Ballot*)value;

- (Invitation*)primitiveReceivedInvitations;
- (void)setPrimitiveReceivedInvitations:(Invitation*)value;

- (NSMutableSet*)primitiveSentInvitations;
- (void)setPrimitiveSentInvitations:(NSMutableSet*)value;

@end
