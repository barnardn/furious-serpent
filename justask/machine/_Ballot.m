// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Ballot.m instead.

#import "_Ballot.h"

const struct BallotAttributes BallotAttributes = {
	.castAt = @"castAt",
	.identity = @"identity",
};

const struct BallotRelationships BallotRelationships = {
	.options = @"options",
	.proposition = @"proposition",
	.voter = @"voter",
};

@implementation BallotID
@end

@implementation _Ballot

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Ballot" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Ballot";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Ballot" inManagedObjectContext:moc_];
}

- (BallotID*)objectID {
	return (BallotID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic castAt;

@dynamic identity;

@dynamic options;

- (NSMutableSet*)optionsSet {
	[self willAccessValueForKey:@"options"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"options"];

	[self didAccessValueForKey:@"options"];
	return result;
}

@dynamic proposition;

@dynamic voter;

@end

