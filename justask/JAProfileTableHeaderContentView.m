//
//  JAProfileTableHeaderContentView.m
//  justask
//
//  Created by Norm Barnard on 11/12/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JustAsk.h"
#import "JAAppearanceManager.h"
#import "JAProfileTableHeaderContentView.h"

@interface JAProfileTableHeaderContentView()

@property (weak, nonatomic) UILabel *titleLabel;

@end

@implementation JAProfileTableHeaderContentView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    UILabel *label = [[UILabel alloc] init];
    label.font = [JAAppearanceManager appearanceManager].cellHeaderFooterFont;
    label.textColor = [JAAppearanceManager appearanceManager].cellHeaderFooterTitleColor;
    [self addSubview:label];
    self.titleLabel = label;
    
 
    RAC(self.titleLabel, text) = [[RACObserve(self, title) deliverOn:[RACScheduler mainThreadScheduler]] map:^id(NSString *value) {
        return [value uppercaseString];
    }];
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.titleLabel.frame = CGRectMake(JASubviewSpacing, 0.0f, CGRectGetWidth(self.bounds) - JASubviewSpacing, CGRectGetHeight(self.bounds));
}

@end
