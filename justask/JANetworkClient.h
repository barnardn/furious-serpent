//
//  JANetworkClient.h
//  justask
//
//  Created by Norm Barnard on 11/2/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import <Foundation/Foundation.h>

@class RACSignal;

@interface JANetworkClient : AFHTTPSessionManager

@property (strong, nonatomic) NSString *sessionToken;

- (RACSignal *)getRequestAtPath:(NSString *)path parameters:(NSDictionary *)parameters;
- (RACSignal *)postRequestAtPath:(NSString *)path parameters:(NSDictionary *)parameters;
- (RACSignal *)putRequestAtPath:(NSString *)path parameters:(NSDictionary *)parameters;
- (RACSignal *)deleteRequestAtPath:(NSString *)path parameters:(NSDictionary *)parameters;

- (RACSignal *)downloadResourceFromURL:(NSURL *)remoteURL toLocalURL:(NSURL *)localURL headers:(NSDictionary *)headers;

+ (instancetype)client;

@end
