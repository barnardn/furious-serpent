//
//  JAAppController.h
//  justask
//
//  Created by Norm Barnard on 2/8/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JAAppController : NSObject

@property (strong, nonatomic, readonly) NSManagedObjectContext *mainManagedObjectContext;

+ (instancetype)appController;

// * app-wide configuration settings

@property (nonatomic, readonly) NSURL *hostAddress;
@property (nonatomic, readonly) NSString *apiKey;
@property (nonatomic, readonly) NSString *avatarUploadToken;

// * local file system methods

- (NSURL *)urlForResourceInApplicationSupport:(NSString *)resourceName;
- (NSURL *)urlForResourceInCache:(NSString *)resourceName;
- (NSURL *)urlForResourceNamed:(NSString *)resourceName inFolderNamed:(NSString *)folderName;
- (NSURL *)urlForResourceInMainBundle:(NSString *)resourceName;

@end
