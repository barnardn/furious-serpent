// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Invitation.h instead.

#import <CoreData/CoreData.h>

extern const struct InvitationAttributes {
	__unsafe_unretained NSString *createdAt;
	__unsafe_unretained NSString *finalizedAt;
	__unsafe_unretained NSString *identity;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *summary;
} InvitationAttributes;

extern const struct InvitationRelationships {
	__unsafe_unretained NSString *creator;
	__unsafe_unretained NSString *invitee;
} InvitationRelationships;

@class Profile;
@class Profile;

@interface InvitationID : NSManagedObjectID {}
@end

@interface _Invitation : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) InvitationID* objectID;

@property (nonatomic, strong) NSDate* createdAt;

//- (BOOL)validateCreatedAt:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* finalizedAt;

//- (BOOL)validateFinalizedAt:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* identity;

//- (BOOL)validateIdentity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* status;

@property (atomic) int16_t statusValue;
- (int16_t)statusValue;
- (void)setStatusValue:(int16_t)value_;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* summary;

//- (BOOL)validateSummary:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Profile *creator;

//- (BOOL)validateCreator:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Profile *invitee;

//- (BOOL)validateInvitee:(id*)value_ error:(NSError**)error_;

@end

@interface _Invitation (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveCreatedAt;
- (void)setPrimitiveCreatedAt:(NSDate*)value;

- (NSDate*)primitiveFinalizedAt;
- (void)setPrimitiveFinalizedAt:(NSDate*)value;

- (NSString*)primitiveIdentity;
- (void)setPrimitiveIdentity:(NSString*)value;

- (NSNumber*)primitiveStatus;
- (void)setPrimitiveStatus:(NSNumber*)value;

- (int16_t)primitiveStatusValue;
- (void)setPrimitiveStatusValue:(int16_t)value_;

- (NSString*)primitiveSummary;
- (void)setPrimitiveSummary:(NSString*)value;

- (Profile*)primitiveCreator;
- (void)setPrimitiveCreator:(Profile*)value;

- (Profile*)primitiveInvitee;
- (void)setPrimitiveInvitee:(Profile*)value;

@end
