//
//  JALegalTermsNavigationController.m
//  justask
//
//  Created by Norm Barnard on 9/2/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JABarButtonConfigurationProtocol.h"
#import "JALegalTermsNavigationController.h"
#import "JALegalTermsTableViewController.h"

@interface JALegalTermsNavigationController ()

@end

@implementation JALegalTermsNavigationController

- (id)init
{
    self = [super init];
    if (!self) return nil;
    
    self.viewControllers = @[ [[JALegalTermsTableViewController alloc] init] ];
    
    return self;
}

- (BOOL)displayRightBarButtonsForChildViewController:(UIViewController *)childViewController; {
    return YES;
}

@end
