//
//  JAWelcomeViewController.m
//  justask
//
//  Created by Norm Barnard on 9/2/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "Account.h"
#import "JustAsk.h"
#import "JAAppController.h"
#import "JAAppearanceManager.h"
#import "JALoginViewController.h"
#import "JASignupNavigationController.h"
#import "JAWelcomeViewController.h"

@interface JAWelcomeViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *signupButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (assign, nonatomic) BOOL shouldDismissOnAppearance;

@end

@implementation JAWelcomeViewController

- (NSString *)nibName {
    return @"JAWelcomeView";
}

- (UIModalTransitionStyle)modalTransitionStyle {
    return UIModalTransitionStyleCrossDissolve;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.shouldDismissOnAppearance = NO;
    self.titleLabel.textColor = [JAAppearanceManager appearanceManager].brandColor;
    self.titleLabel.font = [JAAppearanceManager appearanceManager].appTitleFont;
    self.titleLabel.text = NSLocalizedString(@"Just Ask.", @"welcome view title text");
    
    [self.signupButton setTitleColor:[JAAppearanceManager appearanceManager].blackColor forState:UIControlStateNormal];
    [self.signupButton setTitle:NSLocalizedString(@"New user? Sign up.", @"welcome view signup button title") forState:UIControlStateNormal];
    [self.signupButton setBackgroundImage:[[UIImage imageNamed:@"btn-bg-green"] resizableImageWithCapInsets:UIEdgeInsetsMake(3.0f, 3.0f, 3.0f, 3.0f) ]forState:UIControlStateNormal];
    
    self.loginButton.tintColor = [JAAppearanceManager appearanceManager].bodyTextColor;
    [self.loginButton setTitle:NSLocalizedString(@"Already signed up? Log In.", @"welcome view login button title") forState:UIControlStateNormal];
    [self.loginButton setBackgroundImage:[[UIImage imageNamed:@"btn-bg-regular"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 3, 3, 3)] forState:UIControlStateNormal];
    
    @weakify(self);
    self.signupButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id _) {
        @strongify(self);
        
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            [self presentViewController:[[JASignupNavigationController alloc] init] animated:YES completion:nil];
            self.shouldDismissOnAppearance = YES;
            [subscriber sendCompleted];
            return nil;
        }];
    }];
    
    self.loginButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self);
        
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            [self presentViewController:[[JALoginViewController alloc] init] animated:YES completion:nil];
            self.shouldDismissOnAppearance = YES;
            [subscriber sendCompleted];
            return nil;
        }];
    }];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (!self.shouldDismissOnAppearance) return;
    
    if (![Account currentAccount]) return;
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
    
}



@end
