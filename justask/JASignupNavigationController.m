//
//  JASignupNavigationController.m
//  justask
//
//  Created by Norm Barnard on 8/17/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JASignupNavigationController.h"
#import "JASignupAccountViewController.h"

@interface JASignupNavigationController ()


@end

@implementation JASignupNavigationController

- (id)init
{
    self = [super init];
    if (!self) return nil;
    
    self.viewControllers = @[ [[JASignupAccountViewController alloc] init] ];
    
    return self;
}

- (UIModalPresentationStyle)modalPresentationStyle
{
    return UIModalPresentationFullScreen;
}

@end
