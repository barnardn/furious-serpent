//
//  UIViewController+JAAlertExtensions.m
//  justask
//
//  Created by Norm Barnard on 11/2/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "UIViewController+JAAlertExtensions.h"

@implementation UIViewController (JAAlertExtensions)

- (void)JA_showErrorAlertWithTitle:(NSString *)title message:(NSString *)message completion:(void (^)())completion;
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *dismissAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Dismiss", @"dismiss buton error alert title") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if (completion)
            completion();
    }];
    
    [alert addAction:dismissAction];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
