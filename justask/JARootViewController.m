//
//  JARootViewController.m
//  justask
//
//  Created by Norm Barnard on 2/14/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "Account.h"
#import "JustAsk.h"
#import "JAAppController.h"
#import "JALoginViewController.h"
#import "JARootViewController.h"
#import "JAContentTabBarController.h"
#import "JAFriendsRootViewController.h"
#import "JAVoteDashboardViewController.h"
#import "JAWelcomeViewController.h"

@interface JARootViewController ()

@property (weak, nonatomic) UIViewController *contentViewController;

@end

@implementation JARootViewController

- (id)init
{
    self = [super init];
    return self;
}

- (void)loadView
{
    self.view = [[UIView alloc] init];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    JAContentTabBarController *vcMain = [[JAContentTabBarController alloc] init];
    [self addChildViewController:vcMain];
    [self.view addSubview:vcMain.view];
    [vcMain didMoveToParentViewController:self];
    self.contentViewController = vcMain;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    Account *account = [Account accountInContext:[JAAppController appController].mainManagedObjectContext];
    if (!account)
        [self _showWelcomeView];
}

- (void)viewDidLayoutSubviews
{
    self.contentViewController.view.frame = self.view.bounds;
}

- (void)_showWelcomeView
{
    JAWelcomeViewController *vcWelcome = [[JAWelcomeViewController alloc] init];
    [self presentViewController:vcWelcome animated:YES completion:nil];    
}

- (void)_transitionToMainContentViewController
{
    @weakify(self);
    void (^presentationBlock)() = ^void {
        JAContentTabBarController *vcMain = [[JAContentTabBarController alloc] init];
        [self presentViewController:vcMain animated:YES completion:^{
            @strongify(self);
            self.contentViewController = vcMain;
        }];
    };
    
    if (self.contentViewController) {
        [self.contentViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{
            presentationBlock();
        }];
    } else {
        presentationBlock();
    }
}

@end
