//
//  JAProfileTableHeaderView.h
//  justask
//
//  Created by Norm Barnard on 11/12/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JAProfileTableHeaderView : UITableViewHeaderFooterView

@property (assign, nonatomic) NSString *title;

+ (CGFloat)rowHeight;

@end
