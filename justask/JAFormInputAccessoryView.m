//
//  JAFormInputAccessoryView.m
//  justask
//
//  Created by Norm Barnard on 2/16/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JustAsk.h"
#import "JAFormInputAccessoryView.h"
#import "UIButton+JustAsk.h"

static const CGFloat kPreferredViewHeight = 40.0f;
static const CGFloat kMargin = 4.0f;
static const CGFloat kPadding = 8.0f;
static const CGSize kButtonSize = (CGSize) { .width = 40.0f, .height = 30.0f };

@interface JAFormInputAccessoryView()

@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;
@property (weak, nonatomic) UIButton *doneButton;
@property (strong, nonatomic) RACSubject *buttonTapSubject;

@end


@implementation JAFormInputAccessoryView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self setBackgroundColor:[UIColor lightGrayColor]];
    _buttonTapSubject = [RACSubject subject];
    UIButton *pb = [UIButton JA_ButtonWithImage:[[UIImage imageNamed:@"icon-arrow-bwd"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] frame:CGRectMake(kMargin, 4.0f, kButtonSize.width, kButtonSize.height)];
    [self addSubview:pb];
    
    UIButton *nb = [UIButton JA_ButtonWithImage:[[UIImage imageNamed:@"icon-arrow-fwd"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] frame:CGRectZero];
    [self addSubview:nb];
    
    UIButton *db = [UIButton JA_ButtonWithTitle:@"Done" frame:CGRectZero];
    [self addSubview:db];
    
    [self setPreviousButton:pb];
    [self setNextButton:nb];
    [self setDoneButton:db];
    
    [pb setRac_command:[[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            [[self buttonTapSubject] sendNext:@(FormInputAccessoryButtonPrevious)];
            [subscriber sendCompleted];
            return nil;
        }];
    }]];
    [nb setRac_command:[[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            [[self buttonTapSubject] sendNext:@(FormInputAccessoryButtonNext)];
            [subscriber sendCompleted];
            return nil;
        }];
    }]];
    [db setRac_command:[[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            [[self buttonTapSubject] sendNext:@(FormInputAccessoryButtonDone)];
            [subscriber sendCompleted];
            return nil;
        }];
    }]];
    return self;
}

- (void)layoutSubviews
{
    [[self previousButton] setFrame:CGRectMake(kMargin, 4.0f, kButtonSize.width, kButtonSize.height)];
    [[self nextButton] setFrame:CGRectMake(CGRectGetMaxX([[self previousButton] frame]) + kPadding, 4.0f, kButtonSize.width, kButtonSize.height)];
    [[self doneButton] setFrame:CGRectMake(CGRectGetMaxX([self frame]) - 60.0f, 4.0f, 40.0f, kButtonSize.height)];
}

- (RACSignal *)inputAccessoryButtonTapSignal
{
    return [self buttonTapSubject];
}

#pragma mark - class methods

+ (CGFloat)preferredHeight
{
    return kPreferredViewHeight;
}


@end
