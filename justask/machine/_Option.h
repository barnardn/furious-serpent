// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Option.h instead.

#import <CoreData/CoreData.h>

extern const struct OptionAttributes {
	__unsafe_unretained NSString *identity;
	__unsafe_unretained NSString *summary;
	__unsafe_unretained NSString *writeInText;
} OptionAttributes;

extern const struct OptionRelationships {
	__unsafe_unretained NSString *ballots;
	__unsafe_unretained NSString *proposition;
} OptionRelationships;

@class Ballot;
@class Proposition;

@interface OptionID : NSManagedObjectID {}
@end

@interface _Option : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) OptionID* objectID;

@property (nonatomic, strong) NSString* identity;

//- (BOOL)validateIdentity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* summary;

//- (BOOL)validateSummary:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* writeInText;

//- (BOOL)validateWriteInText:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *ballots;

- (NSMutableSet*)ballotsSet;

@property (nonatomic, strong) Proposition *proposition;

//- (BOOL)validateProposition:(id*)value_ error:(NSError**)error_;

@end

@interface _Option (BallotsCoreDataGeneratedAccessors)
- (void)addBallots:(NSSet*)value_;
- (void)removeBallots:(NSSet*)value_;
- (void)addBallotsObject:(Ballot*)value_;
- (void)removeBallotsObject:(Ballot*)value_;

@end

@interface _Option (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveIdentity;
- (void)setPrimitiveIdentity:(NSString*)value;

- (NSString*)primitiveSummary;
- (void)setPrimitiveSummary:(NSString*)value;

- (NSString*)primitiveWriteInText;
- (void)setPrimitiveWriteInText:(NSString*)value;

- (NSMutableSet*)primitiveBallots;
- (void)setPrimitiveBallots:(NSMutableSet*)value;

- (Proposition*)primitiveProposition;
- (void)setPrimitiveProposition:(Proposition*)value;

@end
