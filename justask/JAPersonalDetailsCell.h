//
//  JAPersonalDetailsCell.h
//  justask
//
//  Created by Norm Barnard on 11/10/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JAProfileViewModel;

@interface JAPersonalDetailsCell : UITableViewCell

@property (strong, nonatomic) JAProfileViewModel *viewModel;

+ (CGFloat)rowHeight;

@end
