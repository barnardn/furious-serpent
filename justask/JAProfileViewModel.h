//
//  JAProfileViewModel.h
//  justask
//
//  Created by Norm Barnard on 8/10/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "RVMViewModel.h"

@class Account;
@class Profile;
@class RACCommand;

@interface JAProfileViewModel : RVMViewModel <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (copy, nonatomic) NSString *firstName;
@property (copy, nonatomic) NSString *lastName;
@property (copy, nonatomic) NSString *fullName;
@property (copy, nonatomic) NSString *nickName;
@property (copy, nonatomic) NSString *city;
@property (copy, nonatomic) NSString *country;
@property (copy, nonatomic) NSURL *avatarURL;
@property (strong, nonatomic) NSDate *updatedAt;
@property (assign, nonatomic) BOOL adhoc;           // non-app using voter (i.e. email)
@property (assign, nonatomic) BOOL approved;        // is this profile an approved friend?
@property (strong, nonatomic, readonly) Profile *profile;
@property (assign, nonatomic, readonly) CGFloat avatarImageLength;

@property (strong, nonatomic,readwrite) NSURL *profileImageURL;
@property (strong, nonatomic,readonly) NSURL *gravatarURL;

@property (assign, nonatomic, getter=isStoringAvatar) BOOL storingAvatar;

@property (strong, nonatomic, readonly) RACCommand *profileImageCommand;
@property (strong, nonatomic, readonly) RACCommand *saveProfileCommand;

- (instancetype)initWithProfile:(Profile *)profile;        // used to create a view model from an existing voter object
- (instancetype)initWithAccount:(Account *)account;  // used during signup to create the account holders voter info

- (RACSignal *)profileImage;

+ (NSString *)avatarFilename;

@end
