//
//  UIViewController+JAAlertExtensions.h
//  justask
//
//  Created by Norm Barnard on 11/2/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (JAAlertExtensions)

- (void)JA_showErrorAlertWithTitle:(NSString *)title message:(NSString *)message completion:(void (^)())completion;

@end
