//
//  JAIndicatorTextField.m
//  justask
//
//  Created by Norm Barnard on 2/22/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JustAsk.h"
#import "JAAppearanceManager.h"
#import "JAIndicatorTextField.h"

@interface JAIndicatorTextField()

@property (nonatomic, readwrite) RACCommand *wrongActionCommand;

@end


@implementation JAIndicatorTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    [self _init];
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self _init];
}

- (void)_init
{
    self.textColor = [JAAppearanceManager appearanceManager].bodyTextColor;
    self.font = [JAAppearanceManager appearanceManager].userInputFont;
    
    self.wrongActionCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            [subscriber sendNext:@(YES)];
            [subscriber sendCompleted];
            return nil;
        }];
    }];
}

- (UIView *)_incorrectView
{
    UIButton *wrongButton = [[UIButton alloc] init];
    UIImage *wrongImage = [[UIImage imageNamed:@"icon-wrong"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [wrongButton setImage:wrongImage forState:UIControlStateNormal];
    [wrongButton setTintColor:[UIColor redColor]];
    wrongButton.rac_command = self.wrongActionCommand;
    return wrongButton;
}

- (UIView *)_correctView
{
    UIImage *wrongImage = [[UIImage imageNamed:@"icon-correct"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:wrongImage];
    [imgView setTintColor:[UIColor greenColor]];
    return imgView;
}

- (CGRect)rightViewRectForBounds:(CGRect)bounds
{
    CGFloat margin = 2.0f;
    return CGRectMake(CGRectGetWidth(bounds) - CGRectGetHeight(bounds) - margin, margin, CGRectGetHeight(bounds) - 2*margin, CGRectGetHeight(bounds) - 2*margin);
}

- (void)setCorrect:(BOOL)correct
{
    _correct = correct;
    if (correct)
        [self setRightView:[self _correctView]];
    else
        [self setRightView:[self _incorrectView]];
    [self setRightViewMode:UITextFieldViewModeAlways];
}

@end
