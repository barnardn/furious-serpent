//
//  JAAppearanceManager.m
//  justask
//
//  Created by Norm Barnard on 2/14/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JAAppearanceManager.h"

@implementation JAAppearanceManager

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    return self;
}

- (void)setupAppearanceProxies
{
    [[UITextField appearance] setTextColor:[self bodyTextColor]];
    [[UITextField appearance] setFont:[self userInputFont]];
    [[UITextField appearance] setTintColor:[UIColor lightGrayColor]];
    [[UIButton appearance] setTintColor:[self buttonTintColor]];
    [[UITabBar appearanceWhenContainedIn:[UITabBarController class], nil] setTintColor:[self buttonTintColor]];
    [[UINavigationBar appearanceWhenContainedIn:[UINavigationController class], nil] setBarTintColor:[self buttonTitleColor]];
    [[UINavigationBar appearanceWhenContainedIn:[UINavigationController class], nil] setTintColor:[self whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
}

- (UIColor *)buttonTitleColor;
{
    return self.brandColor;
}

- (UIColor *)buttonTintColor;
{
    return self.brandColor;
}

- (UIColor *)controlBorderColor
{
    return [UIColor colorWithWhite:0.95f alpha:1.0f];
}

- (UIColor *)bodyTextColor;
{
    return [self blackColor];
}

- (UIColor *)brandColor;
{
    return [UIColor colorWithRed:0.38 green:0.09 blue:0.94 alpha:1.0];
}

- (UIColor *)whiteColor;
{
    return [UIColor whiteColor];
}

- (UIColor *)blackColor;
{
    return [UIColor colorWithRed:0.11f green:0.019f blue:0.26f alpha:1.0f];
}

- (UIColor *)groupedTableViewBackgroundColor {
    return [UIColor colorWithRed:0.95f green:0.95f blue:0.95f alpha:1.0f];
}

- (UIColor *)disabledColor;
{
    return [UIColor colorWithWhite:0.25f alpha:1.0f];
}

- (UIColor *)captionTextColor;
{
    return [UIColor colorWithRed:0.72 green:0.51 blue:0.89 alpha:1.0];
}

- (UIColor *)cellHeaderFooterTitleColor {
    return [UIColor colorWithWhite:0.35f alpha:1.0f];
}


- (UIFont *)userInputFont;
{
    return [UIFont systemFontOfSize:18.0f];
}

- (UIFont *)cellLabelFont
{
    return [UIFont boldSystemFontOfSize:16.0f];
}

- (UIFont *)cellDetailsFont
{
    return self.subTitleFont;
}

- (UIFont *)cellHeaderFooterFont
{
    return [UIFont boldSystemFontOfSize:10.0f];
}

- (UIFont *)captionTextFont;
{
    return [UIFont systemFontOfSize:14.0f];
}

- (UIFont *)smallCaptionTextFont;
{
    return [UIFont systemFontOfSize:12.0f];
}

- (UIFont *)appTitleFont
{
    return [UIFont systemFontOfSize:24.0f];
}

- (UIFont *)titleFont
{
    return [UIFont systemFontOfSize:18.0];
}

- (UIFont *)subTitleFont
{
    return [UIFont systemFontOfSize:14.0f];
}

+ (instancetype)appearanceManager
{
    static JAAppearanceManager *staticInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        staticInstance = [[[self class] alloc] init];
    });
    return staticInstance;
}

@end
