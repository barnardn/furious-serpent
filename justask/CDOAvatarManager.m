//
//  CDOAvatarManager.m
//  justask
//
//  Created by Norm Barnard on 12/21/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <NSData+MEExtensions.h>
#import "CDOAvatarManager.h"

static const CGFloat kTimeoutInterval = 30.0f;
static NSString * const kAmazonACLHeader = @"x-amz-acl";

@interface CDOAvatarManager()

@property (strong, nonatomic) NSString *uploadToken;
@property (strong, nonatomic) NSURLSession *urlSession;
@property (copy, nonatomic) void (^successBlock) (NSURL *avatarURL);
@property (copy, nonatomic) void (^failureBlock) (NSURLResponse *response, NSError *error);
@property (strong, nonatomic) NSString  *oauthHeader;

@end

@implementation CDOAvatarManager

- (instancetype)initWithUploadToken:(NSString *)token
{
    self = [super init];
    if (!self) return nil;
    _uploadToken = token;
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = kTimeoutInterval;
    sessionConfig.timeoutIntervalForResource = 2 * kTimeoutInterval;
    
    _oauthHeader = [NSString stringWithFormat:@"OAuth %@", token];
    _urlSession = [NSURLSession sessionWithConfiguration:sessionConfig];

    return self;
    
}

- (void)uploadAvatarAtURL:(NSURL *)fileURL success:(void(^)(NSURL *avatarURL))successBlock failure:(void(^)(NSURLResponse *response, NSError *error))failureBlock;
{
    NSParameterAssert(fileURL);
    NSParameterAssert(successBlock);
    NSParameterAssert(failureBlock);

    self.successBlock = successBlock;
    self.failureBlock = failureBlock;
    
    NSString *requestLink = [NSString stringWithFormat:@"http://avatars.io/v1/token"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestLink]];
    request.HTTPMethod = @"POST";
    request.allHTTPHeaderFields = @{ @"Authorization" : self.oauthHeader, @"Accept" : @"application/json", @"Content-Type" : @"application/json"};
    NSError *jsonError;
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:[self _preuploadPayloadWithURL:fileURL] options:0 error:&jsonError];
    
    __weak CDOAvatarManager *weakSelf = self;
    NSURLSessionDataTask *task = [self.urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        __strong CDOAvatarManager *strongSelf = weakSelf;
        if (error) {
            strongSelf.failureBlock(response, error);
        } else {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:NULL];
            [strongSelf _uploadFile:fileURL avatarInfo:json[@"data"]];
        }
    }];
    
    [task resume];
    
}

#pragma mark - private methods

- (NSDictionary *)_preuploadPayloadWithURL:(NSURL *)url {
    
    NSData *avatarData = [NSData dataWithContentsOfURL:url];
    NSDictionary *data = @{
      @"filename" : url.lastPathComponent,
      @"md5" : [avatarData ME_MD5String],
      @"size" : @([avatarData length]),
      @"path" : self.applicationPath ?: [NSNull null]
    };
    
    return @{@"data": data};
}

- (void)_uploadFile:(NSURL *)fileURL avatarInfo:(NSDictionary *)avatarInfo
{
    NSParameterAssert(fileURL);
    NSParameterAssert(avatarInfo);
    
    NSURL *uploadURL = [NSURL URLWithString:[avatarInfo valueForKeyPath:@"upload_info.upload_url"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:uploadURL];
    request.allHTTPHeaderFields = @{@"Authorization" : [avatarInfo valueForKeyPath:@"upload_info.signature"],
                                    @"Date" : [avatarInfo valueForKeyPath:@"upload_info.date"],
                                    @"Content-Type" : [avatarInfo valueForKeyPath:@"upload_info.content_type"],
                                    kAmazonACLHeader : @"public-read"};
    request.HTTPMethod = @"PUT";
    
    __weak CDOAvatarManager *weakSelf = self;
    NSURLSessionUploadTask *task = [self.urlSession uploadTaskWithRequest:request fromFile:fileURL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        __strong CDOAvatarManager *strongSelf = weakSelf;
        if (error) {
            strongSelf.failureBlock(response, error);
        } else {
            [strongSelf _completeUploadForAvatar:avatarInfo];
        }
        
    }];
    
    [task resume];
    
}

- (void)_completeUploadForAvatar:(NSDictionary *)avatarInfo
{
    NSParameterAssert(avatarInfo);
    
    NSString *completionLink = [NSString stringWithFormat:@"http://avatars.io/v1/token/%@/complete", avatarInfo[@"id"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:completionLink]];
    request.HTTPMethod = @"POST";
    request.allHTTPHeaderFields = @{ @"Authorization" : self.oauthHeader };
    
    __weak CDOAvatarManager *weakSelf = self;
    NSURLSessionDataTask *task = [self.urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        __strong CDOAvatarManager *strongSelf = weakSelf;
        if (error) {
            strongSelf.failureBlock(response, error);
        } else {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:NULL];
            NSURL *avatarURL = [NSURL URLWithString:[json valueForKeyPath:@"data.data"]];
            strongSelf.successBlock(avatarURL);
        }
    }];
    [task resume];
}




@end
