//
//  NSString+InputValidation.h
//  justask
//
//  Created by Norm Barnard on 6/29/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (InputValidation)

- (BOOL)isValidRFC2822EmailAddress;
- (BOOL)isValidPasswordOrViolatingRules:(NSArray * __autoreleasing *)ruleViolations;

@end
