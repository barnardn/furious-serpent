//
//  JARootViewController.h
//  justask
//
//  Created by Norm Barnard on 2/14/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JABaseViewController.h"

@interface JARootViewController : JABaseViewController

@end
