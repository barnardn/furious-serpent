// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Proposition.h instead.

#import <CoreData/CoreData.h>

extern const struct PropositionAttributes {
	__unsafe_unretained NSString *allowVotersToChangeBallot;
	__unsafe_unretained NSString *allowWriteInOptions;
	__unsafe_unretained NSString *expiresAt;
	__unsafe_unretained NSString *identity;
	__unsafe_unretained NSString *maxAllowedSelections;
	__unsafe_unretained NSString *summary;
	__unsafe_unretained NSString *template;
} PropositionAttributes;

extern const struct PropositionRelationships {
	__unsafe_unretained NSString *ballots;
	__unsafe_unretained NSString *options;
} PropositionRelationships;

@class Ballot;
@class Option;

@interface PropositionID : NSManagedObjectID {}
@end

@interface _Proposition : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) PropositionID* objectID;

@property (nonatomic, strong) NSNumber* allowVotersToChangeBallot;

@property (atomic) BOOL allowVotersToChangeBallotValue;
- (BOOL)allowVotersToChangeBallotValue;
- (void)setAllowVotersToChangeBallotValue:(BOOL)value_;

//- (BOOL)validateAllowVotersToChangeBallot:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* allowWriteInOptions;

@property (atomic) BOOL allowWriteInOptionsValue;
- (BOOL)allowWriteInOptionsValue;
- (void)setAllowWriteInOptionsValue:(BOOL)value_;

//- (BOOL)validateAllowWriteInOptions:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* expiresAt;

//- (BOOL)validateExpiresAt:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* identity;

//- (BOOL)validateIdentity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* maxAllowedSelections;

@property (atomic) int16_t maxAllowedSelectionsValue;
- (int16_t)maxAllowedSelectionsValue;
- (void)setMaxAllowedSelectionsValue:(int16_t)value_;

//- (BOOL)validateMaxAllowedSelections:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* summary;

//- (BOOL)validateSummary:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* template;

@property (atomic) BOOL templateValue;
- (BOOL)templateValue;
- (void)setTemplateValue:(BOOL)value_;

//- (BOOL)validateTemplate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *ballots;

- (NSMutableSet*)ballotsSet;

@property (nonatomic, strong) NSSet *options;

- (NSMutableSet*)optionsSet;

@end

@interface _Proposition (BallotsCoreDataGeneratedAccessors)
- (void)addBallots:(NSSet*)value_;
- (void)removeBallots:(NSSet*)value_;
- (void)addBallotsObject:(Ballot*)value_;
- (void)removeBallotsObject:(Ballot*)value_;

@end

@interface _Proposition (OptionsCoreDataGeneratedAccessors)
- (void)addOptions:(NSSet*)value_;
- (void)removeOptions:(NSSet*)value_;
- (void)addOptionsObject:(Option*)value_;
- (void)removeOptionsObject:(Option*)value_;

@end

@interface _Proposition (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAllowVotersToChangeBallot;
- (void)setPrimitiveAllowVotersToChangeBallot:(NSNumber*)value;

- (BOOL)primitiveAllowVotersToChangeBallotValue;
- (void)setPrimitiveAllowVotersToChangeBallotValue:(BOOL)value_;

- (NSNumber*)primitiveAllowWriteInOptions;
- (void)setPrimitiveAllowWriteInOptions:(NSNumber*)value;

- (BOOL)primitiveAllowWriteInOptionsValue;
- (void)setPrimitiveAllowWriteInOptionsValue:(BOOL)value_;

- (NSDate*)primitiveExpiresAt;
- (void)setPrimitiveExpiresAt:(NSDate*)value;

- (NSString*)primitiveIdentity;
- (void)setPrimitiveIdentity:(NSString*)value;

- (NSNumber*)primitiveMaxAllowedSelections;
- (void)setPrimitiveMaxAllowedSelections:(NSNumber*)value;

- (int16_t)primitiveMaxAllowedSelectionsValue;
- (void)setPrimitiveMaxAllowedSelectionsValue:(int16_t)value_;

- (NSString*)primitiveSummary;
- (void)setPrimitiveSummary:(NSString*)value;

- (NSNumber*)primitiveTemplate;
- (void)setPrimitiveTemplate:(NSNumber*)value;

- (BOOL)primitiveTemplateValue;
- (void)setPrimitiveTemplateValue:(BOOL)value_;

- (NSMutableSet*)primitiveBallots;
- (void)setPrimitiveBallots:(NSMutableSet*)value;

- (NSMutableSet*)primitiveOptions;
- (void)setPrimitiveOptions:(NSMutableSet*)value;

@end
