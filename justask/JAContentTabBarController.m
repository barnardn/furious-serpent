//
//  JAContentTabBarController.m
//  justask
//
//  Created by Norm Barnard on 11/10/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JAAppearanceManager.h"
#import "JAContentTabBarController.h"
#import "JAFriendsRootViewController.h"
#import "JAVoteDashboardViewController.h"
#import "JASettingsNavigationController.h"

@interface JAContentTabBarController ()

@end

@implementation JAContentTabBarController

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.tabBar.tintColor = [JAAppearanceManager appearanceManager].brandColor;
    JAVoteDashboardViewController *voteViewController = [[JAVoteDashboardViewController alloc] init];
    JAFriendsRootViewController *friendsViewController = [[JAFriendsRootViewController alloc] init];
    JASettingsNavigationController *settingsViewController = [[JASettingsNavigationController alloc] init];
    
    self.viewControllers = @[voteViewController, friendsViewController, settingsViewController];
    
    return self;
}

- (UIModalTransitionStyle)modalTransitionStyle {
    return UIModalTransitionStyleCrossDissolve;
}

@end
