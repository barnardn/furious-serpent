//
//  NSDate+PostgresTimestampExtensions.m
//  justask
//
//  Created by Norm Barnard on 12/22/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "NSDate+PostgresTimestampExtensions.h"

@implementation NSDate (PostgresTimestampExtensions)


- (unsigned long long)postgresTimestamp;
{
    return self.timeIntervalSince1970 * 1000;
}

+ (NSDate *)dateFromPostgresTimestamp:(unsigned long long)timestamp;
{
    return [NSDate dateWithTimeIntervalSince1970:timestamp/1000];
}
@end
