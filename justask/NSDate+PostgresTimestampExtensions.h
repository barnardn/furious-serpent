//
//  NSDate+PostgresTimestampExtensions.h
//  justask
//
//  Created by Norm Barnard on 12/22/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (PostgresTimestampExtensions)

- (unsigned long long)postgresTimestamp;

+ (NSDate *)dateFromPostgresTimestamp:(unsigned long long)timestamp;

@end
