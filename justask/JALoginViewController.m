//
//  JALoginViewController.m
//  justask
//
//  Created by Norm Barnard on 2/14/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <SVProgressHUD/SVProgressHUD.h>
#import <MEKit/UIViewController+MEExtensions.h>

#import "JustAsk.h"
#import "JADataModel.h"
#import "JAAppController.h"
#import "JAAppearanceManager.h"
#import "JALoginViewController.h"
#import "JALoginViewModel.h"
#import "JAIndicatorTextField.h"
#import "NSString+InputValidation.h"
#import "UIViewController+JAAlertExtensions.h"

@interface JALoginViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet JAIndicatorTextField *usernameField;
@property (weak, nonatomic) IBOutlet JAIndicatorTextField *passwordField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (strong, nonatomic) JALoginViewModel *viewModel;

@end

@implementation JALoginViewController

- (id)init
{
    self = [super init];
    if (!self) return nil;
    
    _viewModel = [[JALoginViewModel alloc] init];
    
    return self;
}

- (NSString *)nibName
{
    return @"JALoginView";
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    Account *account = [Account currentAccount];
    self.usernameField.text = account.email;
    self.viewModel.email = account.email;
    
    RACChannelTo(self.viewModel, email) = self.usernameField.rac_newTextChannel;
    RACChannelTo(self.viewModel, password) = self.passwordField.rac_newTextChannel;
    
    self.titleLabel.font = [JAAppearanceManager appearanceManager].appTitleFont;
    self.titleLabel.textColor = [JAAppearanceManager appearanceManager].brandColor;
    
    self.subtitleLabel.font = [JAAppearanceManager appearanceManager].subTitleFont;
    self.subtitleLabel.textColor = [JAAppearanceManager appearanceManager].brandColor;
    
    self.loginButton.titleLabel.font = [JAAppearanceManager appearanceManager].titleFont;
    [self.loginButton setTitleColor:[JAAppearanceManager appearanceManager].blackColor forState:UIControlStateNormal];
    [self.loginButton setTitleColor:[JAAppearanceManager appearanceManager].disabledColor forState:UIControlStateDisabled];
    [self.loginButton setBackgroundImage:[[UIImage imageNamed:@"btn-bg-green"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 3, 3, 3)] forState:UIControlStateNormal];
    [self.loginButton setBackgroundImage:[[UIImage imageNamed:@"btn-bg-disabled"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 3, 3, 3)] forState:UIControlStateDisabled];
    
    self.forgotPasswordButton.titleLabel.font = [JAAppearanceManager appearanceManager].titleFont;
    [self.forgotPasswordButton setTitleColor:[JAAppearanceManager appearanceManager].blackColor forState:UIControlStateNormal];
    [self.forgotPasswordButton setBackgroundImage:[[UIImage imageNamed:@"btn-bg-regular"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 3, 3, 3)] forState:UIControlStateNormal];
    
    
    [[self usernameField] setPlaceholder:NSLocalizedString(@"Email Address", @"login email address placeholder")];
    [[self passwordField] setPlaceholder:NSLocalizedString(@"Password", @"login password placeholder")];

    self.loginButton.rac_command = self.viewModel.loginCommand;
    
    @weakify(self);

    [[self.usernameField.wrongActionCommand.executionSignals concat] subscribeNext:^(id _) {
        @strongify(self);
        [self JA_showErrorAlertWithTitle:NSLocalizedString(@"Invalid Email Address", @"bad email address error alert title") message:NSLocalizedString(@"The email address you entered is incorrect. Your email address should look like 'wshakespeare@globetheater.org'", @"signup create account bad email address error message") completion:nil];
    }];
    
    [[[self rac_signalForSelector:@selector(textFieldDidEndEditing:) fromProtocol:@protocol(UITextFieldDelegate)] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(RACTuple *tuple) {
         @strongify(self);
         RACTupleUnpack(JAIndicatorTextField *tf) = tuple;
         if (tf == self.usernameField) {
             tf.correct = [self.usernameField.text isValidRFC2822EmailAddress];
         }
    }];

    [[[self rac_signalForSelector:@selector(textFieldDidBeginEditing:) fromProtocol:@protocol(UITextFieldDelegate)] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(RACTuple *tuple) {
         @strongify(self);
         RACTupleUnpack(JAIndicatorTextField *tf) = tuple;
         if (tf == self.usernameField) {
             tf.rightView = nil;
         }
     }];

    [[self.loginButton.rac_command.executionSignals concat] subscribeNext:^(id _) {
        @strongify(self);
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [self.loginButton.rac_command.errors subscribeNext:^(NSError *error) {
        @strongify(self);
        [self JA_showErrorAlertWithTitle:NSLocalizedString(@"Unable To Login", @"login view can't login alert title") message:error.localizedDescription completion:nil];
    }];

    self.forgotPasswordButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            MELogObject(@"show password forgot view");
            return nil;
        }];
    }];
    
    [[self.keyboardWillShowSignal deliverOn:[RACScheduler mainThreadScheduler]]
     
    subscribeNext:^(RACTuple *keyboardInfo) {
        @strongify(self);
        RACTupleUnpack(NSValue *frameValue, NSNumber *animDuration, NSNumber *animCurve) = keyboardInfo;
        
        CGRect kbdEndFrame = [frameValue CGRectValue];
        CGFloat overlap = CGRectGetMaxY(self.passwordField.frame) - CGRectGetMinY(kbdEndFrame);
        if (overlap <= 0) return ;
        
        @weakify(self);
        [UIView animateWithDuration:animDuration.doubleValue delay:0.0f options:animCurve.unsignedIntegerValue animations:^{
            @strongify(self)
            self.view.transform = CGAffineTransformMakeTranslation(0.0f, -overlap);
        } completion:nil];
    }];
    
    [[self.keyboardWillHideSignal deliverOn:[RACScheduler mainThreadScheduler]]
     
    subscribeNext:^(RACTuple *keyboardInfo) {
        @strongify(self);
        RACTupleUnpack(__unused NSValue *frameValue, NSNumber *animDuration, NSNumber *animCurve) = keyboardInfo;
        
        [UIView animateWithDuration:animDuration.doubleValue delay:0.0f options:animCurve.unsignedIntegerValue animations:^{
            self.view.transform = CGAffineTransformIdentity;
        } completion:nil];
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.usernameField)
        [self.passwordField becomeFirstResponder];
    else
        [textField resignFirstResponder];
    return NO;
}




@end
