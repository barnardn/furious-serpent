// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Ballot.h instead.

#import <CoreData/CoreData.h>

extern const struct BallotAttributes {
	__unsafe_unretained NSString *castAt;
	__unsafe_unretained NSString *identity;
} BallotAttributes;

extern const struct BallotRelationships {
	__unsafe_unretained NSString *options;
	__unsafe_unretained NSString *proposition;
	__unsafe_unretained NSString *voter;
} BallotRelationships;

@class Option;
@class Proposition;
@class Profile;

@interface BallotID : NSManagedObjectID {}
@end

@interface _Ballot : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BallotID* objectID;

@property (nonatomic, strong) NSDate* castAt;

//- (BOOL)validateCastAt:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* identity;

//- (BOOL)validateIdentity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *options;

- (NSMutableSet*)optionsSet;

@property (nonatomic, strong) Proposition *proposition;

//- (BOOL)validateProposition:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Profile *voter;

//- (BOOL)validateVoter:(id*)value_ error:(NSError**)error_;

@end

@interface _Ballot (OptionsCoreDataGeneratedAccessors)
- (void)addOptions:(NSSet*)value_;
- (void)removeOptions:(NSSet*)value_;
- (void)addOptionsObject:(Option*)value_;
- (void)removeOptionsObject:(Option*)value_;

@end

@interface _Ballot (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveCastAt;
- (void)setPrimitiveCastAt:(NSDate*)value;

- (NSString*)primitiveIdentity;
- (void)setPrimitiveIdentity:(NSString*)value;

- (NSMutableSet*)primitiveOptions;
- (void)setPrimitiveOptions:(NSMutableSet*)value;

- (Proposition*)primitiveProposition;
- (void)setPrimitiveProposition:(Proposition*)value;

- (Profile*)primitiveVoter;
- (void)setPrimitiveVoter:(Profile*)value;

@end
