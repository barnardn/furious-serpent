//
//  JATextFieldTableViewCell.m
//  justask
//
//  Created by Norm Barnard on 9/1/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JustAsk.h"
#import "JATextFieldTableViewCell.h"

@interface JATextFieldTableViewCell() <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UITextField *valueTextField;

@end


@implementation JATextFieldTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.valueTextField.text = nil;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.separatorInset = UIEdgeInsetsZero;
    self.nameLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.valueTextField.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    RAC(self.nameLabel, text) = [RACObserve(self, name) deliverOn:[RACScheduler mainThreadScheduler]];
    RAC(self.valueTextField, placeholder) = [RACObserve(self, placeholderValue) deliverOn:[RACScheduler mainThreadScheduler]];
    
    @weakify(self);
    [[[[[RACSignal combineLatest:@[RACObserve(self, viewModel), RACObserve(self, valueKeyPath)]]
     filter:^BOOL(RACTuple *tuple) {
         
         RACTupleUnpack(id viewModel, NSString *valueKeyPath) = tuple;
         return (viewModel != nil && valueKeyPath.length > 0);
         
     }]
    deliverOn:[RACScheduler mainThreadScheduler]]
     takeUntil:self.rac_prepareForReuseSignal]
    subscribeNext:^(RACTuple *tuple) {
        @strongify(self)
        RACTupleUnpack(id viewModel, NSString *valueKeyPath) = tuple;
        self.valueTextField.text = [viewModel valueForKeyPath:valueKeyPath];
        
    }];
    
    [[[self.valueTextField.rac_textSignal deliverOn:[RACScheduler mainThreadScheduler]] takeUntil:self.rac_prepareForReuseSignal] subscribeNext:^(NSString *text) {
        @strongify(self);
        
        [self.viewModel setValue:text forKeyPath:self.valueKeyPath];
    }];    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.nameLabel.frame = CGRectMake(JASubviewMargin, 0.0f, self.nameLabelAlignmentWidth, CGRectGetHeight(self.bounds));
    self.valueTextField.frame = CGRectMake(CGRectGetMaxX(self.nameLabel.frame) + JASubviewSpacing,  0.0f, CGRectGetMaxX(self.bounds) - (CGRectGetMaxX(self.nameLabel.frame) + JASubviewSpacing) - JASubviewMargin, CGRectGetHeight(self.bounds));
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    if (self.valueTextField.isFirstResponder)
        [self.valueTextField resignFirstResponder];
}


- (void)setNameLabelAlignmentWidth:(CGFloat)nameLabelAlignmentWidth
{
    _nameLabelAlignmentWidth = nameLabelAlignmentWidth;
    [self setNeedsLayout];
}

@end
