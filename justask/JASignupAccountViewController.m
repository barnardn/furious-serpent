//
//  JASignupAccountViewController.m
//  justask
//
//  Created by Norm Barnard on 9/2/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "Account.h"
#import "FTHTTPCodes.h"
#import "JustAsk.h"
#import "JAAppController.h"
#import "JAAppearanceManager.h"
#import "JALegalTermsNavigationController.h"
#import "JALoginViewController.h"
#import "JAIndicatorTextField.h"
#import "JASignupAccountViewController.h"
#import "JASignupViewModel.h"
#import "NSString+InputValidation.h"
#import "UIViewController+JAAlertExtensions.h"

@interface JASignupAccountViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet JAIndicatorTextField *emailField;
@property (weak, nonatomic) IBOutlet JAIndicatorTextField *passwordField;
@property (weak, nonatomic) IBOutlet UIButton *passwordVisiblityButton;
@property (weak, nonatomic) IBOutlet UIButton *termsButton;
@property (weak, nonatomic) IBOutlet UIButton *createAccountButton;
@property (weak, nonatomic) IBOutlet UIView *termsBackgroundView;

@property (strong, nonatomic) JASignupViewModel *viewModel;
@property (strong, nonatomic) NSArray *passwordStrengthViolations;

@property (strong, nonatomic) NSManagedObjectContext *context;

@end

@implementation JASignupAccountViewController

- (id)init
{
    self = [super init];
    if (!self) return nil;
    
    _context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    _context.undoManager = nil;
    _context.parentContext = [JAAppController appController].mainManagedObjectContext;
    _viewModel = [[JASignupViewModel alloc] init];
    return self;
}

- (NSString *)nibName
{
    return @"JASignupAccountView";
}

- (NSString *)title
{
    return NSLocalizedString(@"Sign Up", @"signup create account view title");
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    RAC(self.viewModel, email) = self.emailField.rac_textSignal;
    RAC(self.viewModel, password) = self.passwordField.rac_textSignal;
        
    self.emailField.rightViewMode = UITextFieldViewModeUnlessEditing;
    self.passwordField.rightViewMode = UITextFieldViewModeUnlessEditing;

    [self.passwordVisiblityButton setImage:[[UIImage imageNamed:@"icon-eye"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.passwordVisiblityButton.tintColor = [JAAppearanceManager appearanceManager].brandColor;
    
    self.titleLabel.font = [JAAppearanceManager appearanceManager].appTitleFont;
    self.titleLabel.textColor = [JAAppearanceManager appearanceManager].brandColor;
    
    self.subtitleLabel.font = [JAAppearanceManager appearanceManager].subTitleFont;
    self.subtitleLabel.textColor = [JAAppearanceManager appearanceManager].brandColor;
    
    [self.termsButton setAttributedTitle:[self _termsButonAttributedTitle] forState:UIControlStateNormal];
    
    self.createAccountButton.titleLabel.font = [JAAppearanceManager appearanceManager].titleFont;
    [self.createAccountButton setTitleColor:[JAAppearanceManager appearanceManager].blackColor forState:UIControlStateNormal];
    [self.createAccountButton setTitleColor:[JAAppearanceManager appearanceManager].disabledColor forState:UIControlStateDisabled];
    [self.createAccountButton setTitle:NSLocalizedString(@"Create Account", @"create account button title") forState:UIControlStateNormal];
    [self.createAccountButton setBackgroundImage:[[UIImage imageNamed:@"btn-bg-green"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 3, 3, 3)] forState:UIControlStateNormal];
    [self.createAccountButton setBackgroundImage:[[UIImage imageNamed:@"btn-bg-disabled"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 3, 3, 3)] forState:UIControlStateDisabled];
    self.termsBackgroundView.backgroundColor = [JAAppearanceManager appearanceManager].controlBorderColor;
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:nil action:nil];
    
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    @weakify(self);
    cancelButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id _) {
        @strongify(self);
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            
            [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            [subscriber sendCompleted];
            return nil;
        }];
    }];
    self.passwordVisiblityButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(UIButton *button) {
        @strongify(self);
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            
            self.passwordField.secureTextEntry = !self.passwordField.isSecureTextEntry;
            if (self.passwordField.secureTextEntry)
                [button setImage:[[UIImage imageNamed:@"icon-eye"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            else
                [button setImage:[[UIImage imageNamed:@"icon-key"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            [subscriber sendCompleted];
            return nil;
        }];
    }];
    
    self.termsButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self);
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            [self presentViewController:[[JALegalTermsNavigationController alloc] init] animated:YES completion:nil];
            return nil;
        }];
    }];

    self.createAccountButton.rac_command = self.viewModel.signupCommand;
    
    [[self.createAccountButton.rac_command.executionSignals concat] subscribeNext:^(id _) {
        @strongify(self);
        JALoginViewController *vcLogin = [[JALoginViewController alloc] init];
        vcLogin.navigationItem.hidesBackButton = YES;
        [self.navigationController pushViewController:vcLogin animated:YES];        
    }];
    
    [self.createAccountButton.rac_command.errors subscribeNext:^(NSError *error) {
        @strongify(self);
        NSString *msg = error.localizedDescription;
        if (error.code == HTTPCode409Conflict)
            msg = NSLocalizedString(@"The email address you provided is being used by another account. Try again with a different email addres", @"duplicate email address error");
        [self JA_showErrorAlertWithTitle:NSLocalizedString(@"Error Creating Account", @"signup view account creation alert title") message:msg completion:nil];
    }];
    
    [[self.emailField.wrongActionCommand.executionSignals concat] subscribeNext:^(id _) {
        @strongify(self);
        [self JA_showErrorAlertWithTitle:NSLocalizedString(@"Invalid Email Address", @"bad email address error alert title") message:NSLocalizedString(@"The email address you entered is incorrect. Your email address should look like 'wshakespeare@globetheater.org'", @"signup create account bad email address error message") completion:nil];
    }];
    
    [[self.passwordField.wrongActionCommand.executionSignals concat] subscribeNext:^(id _) {
        @strongify(self);
        NSString *message = [NSString stringWithFormat:NSLocalizedString(@"The password is too weak for the following reasons: %@", @"bad passsword format string"), [self.passwordStrengthViolations componentsJoinedByString:@"\n"]];
        [self JA_showErrorAlertWithTitle:NSLocalizedString(@"Bad Password", @"bad password error alert title") message:message completion:nil];
    }];
    
    [[[self rac_signalForSelector:@selector(textFieldDidEndEditing:) fromProtocol:@protocol(UITextFieldDelegate)]
     deliverOn:[RACScheduler mainThreadScheduler]]
    subscribeNext:^(RACTuple *tuple) {
        @strongify(self);
        RACTupleUnpack(JAIndicatorTextField *textField) = tuple;
        
        if (textField == self.emailField) {
            textField.correct = [textField.text isValidRFC2822EmailAddress];
        } else {
            NSArray *ruleViolations;
            textField.correct = [textField.text isValidPasswordOrViolatingRules:&ruleViolations];
            if (!textField.isCorrect)
                self.passwordStrengthViolations = ruleViolations;
            else
                self.passwordStrengthViolations = nil;
        }
    }];
    
    [[[self rac_signalForSelector:@selector(textFieldDidBeginEditing:) fromProtocol:@protocol(UITextFieldDelegate)]
    deliverOn:[RACScheduler mainThreadScheduler]]
    subscribeNext:^(RACTuple *tuple) {
        RACTupleUnpack(JAIndicatorTextField *textField) = tuple;
        textField.rightView = nil;
    }];
    
}

#pragma mark - textfield delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.emailField)
        [self.passwordField becomeFirstResponder];
    else
        [textField resignFirstResponder];
    return NO;
}

#pragma mark - private methods

- (RACSignal *)_canSubmitSignal
{
    return [RACSignal combineLatest:@[self.emailField.rac_textSignal, self.passwordField.rac_textSignal] reduce:^id (NSString *email, NSString *password) {
        return @([email isValidRFC2822EmailAddress] && [password isValidPasswordOrViolatingRules:NULL]);
    }];
}

- (NSAttributedString *)_termsButonAttributedTitle
{
    NSMutableAttributedString *agreeString = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"By clicking \"Create Account\", you agree to our ", @"signup new account terms text ") attributes:@{ NSFontAttributeName : [JAAppearanceManager appearanceManager].smallCaptionTextFont, NSForegroundColorAttributeName : [JAAppearanceManager appearanceManager].blackColor}];
    
   
    NSAttributedString *terms = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Terms and Conditions", @"signup new account underlined terms and conditions") attributes:@{ NSFontAttributeName : [JAAppearanceManager appearanceManager].smallCaptionTextFont, NSForegroundColorAttributeName : [JAAppearanceManager appearanceManager].brandColor, NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)}];
    
    NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithAttributedString:agreeString];
    [title appendAttributedString:terms];
    return title;
}


@end
