//
//  JALegalTermsTableViewController.m
//  justask
//
//  Created by Norm Barnard on 9/2/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JustAsk.h"
#import "JAAppearanceManager.h"
#import "JAAppController.h"
#import "JABarButtonConfigurationProtocol.h"
#import "JALegalTermsTableViewController.h"
#import "JALegalTextViewController.h"

static NSString * const kAboutDocumentFilename = @"About.rtf";
static NSString * const kTermsDocumentFilename = @"TermsNConditions.rtf";
static NSString * const kPrivacyDocumentFilename = @"Privacy.rtf";

typedef NS_ENUM(NSInteger, LegalSectionIndex) {
    LegalSectionIndexAbout,
    LegalSectionIndexTerms,
    LegalSectionIndexPrivacy,
    LegalSectionIndexEndMarker
};

@interface JALegalTermsTableViewController ()

@end

@implementation JALegalTermsTableViewController

- (instancetype)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (!self) return nil;
    return self;
}

- (NSString *)title
{
    return NSLocalizedString(@"Legal", @"legal view navigation bar title");
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
    self.tableView.backgroundColor = [JAAppearanceManager appearanceManager].groupedTableViewBackgroundColor;
    
    if ([self.navigationController conformsToProtocol:@protocol(JABarButtonConfigurationProtocol)]) {
        
        id<JABarButtonConfigurationProtocol> navController = (id<JABarButtonConfigurationProtocol>)self.navigationController;
        
        if ([navController displayRightBarButtonsForChildViewController:self]) {
            UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:nil action:nil];
            doneButton.tintColor = [JAAppearanceManager appearanceManager].whiteColor;
            
            self.navigationItem.rightBarButtonItem  = doneButton;
            
            @weakify(self);
            doneButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
                @strongify(self);
                return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
                    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
                    return nil;
                }];
            }];
        }
    }
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return LegalSectionIndexEndMarker;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class]) forIndexPath:indexPath];
    cell.textLabel.font = [JAAppearanceManager appearanceManager].titleFont;
    cell.textLabel.textColor = [JAAppearanceManager appearanceManager].bodyTextColor;
    cell.textLabel.text = [self _titleForIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}


#pragma mark - tableview delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *filenames = @[kAboutDocumentFilename, kTermsDocumentFilename, kPrivacyDocumentFilename];
    NSURL *documentURL = [[JAAppController appController] urlForResourceInMainBundle:filenames[indexPath.section]];
    JALegalTextViewController *vcText = [[JALegalTextViewController alloc] initWithLegalDocumentURL:documentURL title:[self _titleForIndexPath:indexPath]];
    [self.navigationController pushViewController:vcText animated:YES];
}

#pragma mark - private methods

- (NSString *)_titleForIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case LegalSectionIndexAbout:
            return NSLocalizedString(@"About", @"legal view about cell title");
        case LegalSectionIndexTerms:
            return  NSLocalizedString(@"Terms & Conditions", @"legal view terms and condition tablecell title");
        case LegalSectionIndexPrivacy:
            return NSLocalizedString(@"Privacy Policy", @"legal view privary policy cell title");
        default:
            return nil;
    }
}




@end
