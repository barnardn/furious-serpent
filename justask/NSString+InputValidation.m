//
//  NSString+InputValidation.m
//  justask
//
//  Created by Norm Barnard on 6/29/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <Navajo/NJOPasswordStrengthEvaluator.h>
#import <MEReactiveFoundation/MEReactiveFoundation.h>

#import "NSString+InputValidation.h"

@implementation NSString (InputValidation)

-  (BOOL)isValidRFC2822EmailAddress
{
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

- (BOOL)isValidPasswordOrViolatingRules:(NSArray * __autoreleasing *)ruleViolations
{
    NSArray *rules = @[[NJOLengthRule ruleWithRange:NSMakeRange(8, 64)], [NJORequiredCharacterRule uppercaseCharacterRequiredRule]];

    NJOPasswordValidator *validator = [NJOPasswordValidator validatorWithRules:rules];
    NSArray *violations;
    BOOL valid =  [validator validatePassword:self failingRules:&violations];
    if (!valid && (ruleViolations != NULL)) {
        *ruleViolations = [violations MER_map:^id(id<NJOPasswordRule> rule) {
            return [rule localizedErrorDescription];
        }];
    }
    return valid;
}

@end
