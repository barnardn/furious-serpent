//
//  UIButton+JustAsk.m
//  justask
//
//  Created by Norm Barnard on 2/16/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JAAppearanceManager.h"
#import "UIButton+JustAsk.h"

@implementation UIButton (JustAsk)

+ (UIButton *)JA_ButtonWithTitle:(NSString *)title frame:(CGRect)frame
{
    UIButton *button = [[UIButton alloc] initWithFrame:frame];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTintColor:[[JAAppearanceManager appearanceManager] buttonTintColor]];
    [[button titleLabel] setFont:[[JAAppearanceManager appearanceManager] titleFont]];
    [button setTitleColor:[[JAAppearanceManager appearanceManager] buttonTitleColor] forState:UIControlStateNormal];
    return button;
}

+ (UIButton *)JA_ButtonWithImage:(UIImage *)image frame:(CGRect)frame
{
    UIButton *button = [[UIButton alloc] initWithFrame:frame];
    [button setTintColor:[[JAAppearanceManager appearanceManager]buttonTintColor]];
    [button setImage:image forState:UIControlStateNormal];
    return button;
}

@end
