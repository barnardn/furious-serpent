//
//  JAProfileTableHeaderContentView.h
//  justask
//
//  Created by Norm Barnard on 11/12/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface JAProfileTableHeaderContentView : UIView

@property (copy, nonatomic) NSString *title;

@end
