// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Profile.m instead.

#import "_Profile.h"

const struct ProfileAttributes ProfileAttributes = {
	.adhoc = @"adhoc",
	.approved = @"approved",
	.avatarUrl = @"avatarUrl",
	.city = @"city",
	.country = @"country",
	.createdAt = @"createdAt",
	.firstName = @"firstName",
	.identity = @"identity",
	.lastName = @"lastName",
	.latitude = @"latitude",
	.longitude = @"longitude",
	.nickName = @"nickName",
	.updatedAt = @"updatedAt",
};

const struct ProfileRelationships ProfileRelationships = {
	.account = @"account",
	.ballots = @"ballots",
	.receivedInvitations = @"receivedInvitations",
	.sentInvitations = @"sentInvitations",
};

@implementation ProfileID
@end

@implementation _Profile

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Profile" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Profile";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Profile" inManagedObjectContext:moc_];
}

- (ProfileID*)objectID {
	return (ProfileID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"adhocValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"adhoc"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"approvedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"approved"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic adhoc;

- (BOOL)adhocValue {
	NSNumber *result = [self adhoc];
	return [result boolValue];
}

- (void)setAdhocValue:(BOOL)value_ {
	[self setAdhoc:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveAdhocValue {
	NSNumber *result = [self primitiveAdhoc];
	return [result boolValue];
}

- (void)setPrimitiveAdhocValue:(BOOL)value_ {
	[self setPrimitiveAdhoc:[NSNumber numberWithBool:value_]];
}

@dynamic approved;

- (BOOL)approvedValue {
	NSNumber *result = [self approved];
	return [result boolValue];
}

- (void)setApprovedValue:(BOOL)value_ {
	[self setApproved:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveApprovedValue {
	NSNumber *result = [self primitiveApproved];
	return [result boolValue];
}

- (void)setPrimitiveApprovedValue:(BOOL)value_ {
	[self setPrimitiveApproved:[NSNumber numberWithBool:value_]];
}

@dynamic avatarUrl;

@dynamic city;

@dynamic country;

@dynamic createdAt;

@dynamic firstName;

@dynamic identity;

@dynamic lastName;

@dynamic latitude;

@dynamic longitude;

@dynamic nickName;

@dynamic updatedAt;

@dynamic account;

@dynamic ballots;

@dynamic receivedInvitations;

@dynamic sentInvitations;

- (NSMutableSet*)sentInvitationsSet {
	[self willAccessValueForKey:@"sentInvitations"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"sentInvitations"];

	[self didAccessValueForKey:@"sentInvitations"];
	return result;
}

@end

