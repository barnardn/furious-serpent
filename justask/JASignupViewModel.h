//
//  JASignupViewModel.h
//  justask
//
//  Created by Norm Barnard on 11/2/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "RVMViewModel.h"

@class RACCommand;

@interface JASignupViewModel : RVMViewModel

@property (copy, nonatomic, readwrite) NSString *email;
@property (copy, nonatomic, readwrite) NSString *password;
@property (strong, nonatomic, readonly) RACCommand *signupCommand;


@end
