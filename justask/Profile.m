
#import "JustAsk.h"
#import "Profile.h"



@interface Profile ()

// Private interface goes here.

@end


@implementation Profile

#pragma mark - core data overrides

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    self.createdAt = [NSDate date];
    self.updatedAt = [NSDate date];
}

- (NSDictionary *)jsonSerialization
{
    NSDictionary *json =  @{
         ProfileAttributes.identity : self.identity ?: [NSNull null],
         ProfileAttributes.city : self.city ?: [NSNull null],
         ProfileAttributes.country : self.country ?: [NSNull null],
         ProfileAttributes.firstName : self.firstName ?: [NSNull null],
         ProfileAttributes.lastName : self.lastName ?: [NSNull null],
         ProfileAttributes.nickName : self.nickName ?: [NSNull null],
         ProfileAttributes.avatarUrl : self.avatarUrl ?: [NSNull null]
    };
    
    return [json MER_filter:^BOOL(NSString *key, id value) {
        return (value != [NSNull null]);
    }];
}


@end
