//
//  JASettingsTableViewController.h
//  justask
//
//  Created by Norm Barnard on 12/24/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JASettingsTableViewController : UITableViewController

@end
