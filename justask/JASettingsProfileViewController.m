//
//  JASignupViewController.m
//  justask
//
//  Created by Norm Barnard on 8/10/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <MERThumbnailKit/MERThumbnailKit.h>

#import "CDOAvatarManager.h"
#import "JustAsk.h"
#import "JADataModel.h"
#import "JAAccountViewModel.h"
#import "JAAppController.h"
#import "JAAppearanceManager.h"
#import "JAPersonalDetailsCell.h"
#import "JAProfileTableHeaderView.h"
#import "JASignupTableViewCell.h"
#import "JASettingsProfileViewController.h"
#import "JATextFieldTableViewCell.h"
#import "JAProfileViewModel.h"
#import "NSString+InputValidation.h"

typedef NS_ENUM(NSUInteger, JASignupTableSectionTag) {
    JASignupTableSectionTagPersonal,
    JASignupTableSectionTagLocation,
    JSSignupTableSectionTagPassword
};

typedef NS_ENUM(NSUInteger, JASignupTableRowTag) {
    JASignupTableRowTagPersonal,       // personal
    JASignupTableRowTagLocation,
    JASignupTableRowTagPassword
};


@interface JASettingsProfileViewController () <UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) JAAccountViewModel *viewModel;
@property (strong, nonatomic) NSArray *sectionInfo;
@property (strong, nonatomic) NSArray *sectionTitles;
@property (strong, nonatomic) CDOAvatarManager *avatarManager;

@end

@implementation JASettingsProfileViewController

- (id)init
{
    self = [super init];
    if (!self) return nil;
    
    _context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    _context.undoManager = nil;
    _context.parentContext = [JAAppController appController].mainManagedObjectContext;
    
    NSArray *personalInfo = @[@(JASignupTableRowTagPersonal)];
    NSArray *locationInfo = @[@(JASignupTableRowTagLocation)];
    NSArray *passwordInfo = @[@(JASignupTableRowTagPassword)];
    
    _sectionInfo = @[personalInfo,locationInfo, passwordInfo];
    
    _sectionTitles = @[NSLocalizedString(@"Personal Information", @"signup form personal information section title"),
                       NSLocalizedString(@"Home Location", @"signup form home location section title"),
                       NSLocalizedString(@"Account", @"settings profile account  section title")];
    
    return self;
}

- (NSString *)nibName {
    return @"JASettingsProfileView";
}

- (NSString *)title {
    return NSLocalizedString(@"Profile", @"profile form title");
}

- (UIRectEdge)edgesForExtendedLayout {
    return UIRectEdgeAll ^ UIRectEdgeTop;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    Account *account = [Account currentAccount];
    NSParameterAssert(account);
    self.viewModel = [[JAAccountViewModel alloc] initWithAccount:account];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([JAPersonalDetailsCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([JAPersonalDetailsCell class])];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([JASignupTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([JASignupTableViewCell class])];

    [self.tableView registerClass:[JAProfileTableHeaderView class] forHeaderFooterViewReuseIdentifier:NSStringFromClass([JAProfileTableHeaderView class])];
    
    self.tableView.separatorColor = [JAAppearanceManager appearanceManager].controlBorderColor;
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:nil action:nil];
    
    [doneButton setTitleTextAttributes:@{ NSForegroundColorAttributeName : [JAAppearanceManager appearanceManager].disabledColor } forState:UIControlStateDisabled];
    [doneButton setTitleTextAttributes:@{ NSForegroundColorAttributeName : [JAAppearanceManager appearanceManager].captionTextColor} forState:UIControlStateNormal];
    
    @weakify(self);
    
    doneButton.rac_command = self.viewModel.profileViewModel.saveProfileCommand;

    [doneButton.rac_command.errors subscribeNext:^(NSError *error) {
        MELogObject(error);
    }];
    
    self.navigationItem.rightBarButtonItem = doneButton;
    
    [[[self.viewModel.profileViewModel.profileImageCommand.executionSignals concat]
      takeUntil:self.rac_willDeallocSignal]
     subscribeNext:^(JAProfileViewModel *profileViewModel) {
         @strongify(self);
         [self _selectProfileImageForViewModel:profileViewModel];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

#pragma mark - tableview datasource mehtods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sectionInfo.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *rowInfo = self.sectionInfo[section];
    return rowInfo.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (indexPath.section == 0) {
        JAPersonalDetailsCell *pdCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([JAPersonalDetailsCell class]) forIndexPath:indexPath];
        RAC(pdCell, viewModel) = [[RACSignal return:self.viewModel.profileViewModel] takeUntil:pdCell.rac_prepareForReuseSignal];
        cell = pdCell;
    } else {
        JASignupTableViewCell *ncell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([JASignupTableViewCell class]) forIndexPath:indexPath];
        NSNumber *rowTag = self.sectionInfo[indexPath.section][indexPath.row];
        RAC(ncell, name) = [[RACSignal return:[self _nameForRowTag:rowTag.integerValue]] takeUntil:ncell.rac_prepareForReuseSignal];
        ncell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell = ncell;
    }
    return cell;
}

#pragma mark - tableview delegate methods

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    JAProfileTableHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([JAProfileTableHeaderView class])];
    headerView.title = self.sectionTitles[section];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return [JAProfileTableHeaderView rowHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (indexPath.section == 0) ? [JAPersonalDetailsCell rowHeight] : [JASignupTableViewCell rowHeight];
}

#pragma mark - private methods

- (NSString *)_nameForRowTag:(JASignupTableRowTag)rowTag
{
    switch (rowTag) {
        case JASignupTableRowTagPersonal:
            return NSLocalizedString(@"Personal Details", @"signup form personal details cell title");
        case JASignupTableRowTagLocation:
            return NSLocalizedString(@"Location", @"signup form locaion cell title");
        case JASignupTableRowTagPassword:
            return NSLocalizedString(@"Change Password", @"profile settings change password");
        default:
            MEAssertLog(@"Unknown row tag: %@", @(rowTag));
    }
    return @"";
}

- (void)_selectProfileImageForViewModel:(JAProfileViewModel *)viewModel
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront])
            imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    } else {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    CGFloat length = self.viewModel.profileViewModel.avatarImageLength;
    UIImage *thumbnail = [image MER_thumbnailOfSize:CGSizeMake(length, length)];
    NSData *imageData = UIImageJPEGRepresentation(thumbnail, 0.80f);
    NSURL *profileImageURL = [[JAAppController appController] urlForResourceInApplicationSupport:[JAProfileViewModel avatarFilename]];
    [imageData writeToURL:profileImageURL atomically:YES];
    self.viewModel.profileViewModel.profileImageURL = profileImageURL;
    [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
    CDOAvatarManager *avatarManager = [[CDOAvatarManager alloc] initWithUploadToken:[JAAppController appController].avatarUploadToken];
    avatarManager.applicationPath = @"justask";
    self.viewModel.profileViewModel.storingAvatar = YES;
    [avatarManager uploadAvatarAtURL:profileImageURL success:^(NSURL *avatarURL) {
        self.viewModel.profileViewModel.avatarURL = avatarURL;
        self.viewModel.profileViewModel.storingAvatar = NO;
    } failure:^(NSURLResponse *response, NSError *error) {
        self.viewModel.profileViewModel.storingAvatar = NO;
    }];
    self.avatarManager = avatarManager;
}


@end
