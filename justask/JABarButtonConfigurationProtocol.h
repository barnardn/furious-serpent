//
//  JABarButtonConfigurationProtocol.h
//  justask
//
//  Created by Norm Barnard on 12/26/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIViewController;

@protocol JABarButtonConfigurationProtocol <NSObject>

@optional

- (BOOL)displayRightBarButtonsForChildViewController:(UIViewController *)childViewController;

@end
