//
//  JAFormInputAccessoryView.h
//  justask
//
//  Created by Norm Barnard on 2/16/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, FormInputAccessoryButton) {
    
    FormInputAccessoryButtonNone,
    FormInputAccessoryButtonPrevious,
    FormInputAccessoryButtonNext,
    FormInputAccessoryButtonDone
};

@class RACSignal;

@interface JAFormInputAccessoryView : UIView

- (RACSignal *)inputAccessoryButtonTapSignal;
+ (CGFloat)preferredHeight;

@end
