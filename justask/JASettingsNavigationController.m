//
//  JASettingsNavigationController.m
//  justask
//
//  Created by Norm Barnard on 12/26/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JABarButtonConfigurationProtocol.h"
#import "JASettingsNavigationController.h"
#import "JASettingsTableViewController.h"

@implementation JASettingsNavigationController

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    self.viewControllers = @[[[JASettingsTableViewController alloc] init]];
    return self;
}

- (UITabBarItem *)tabBarItem
{
    UITabBarItem *tbi = [super tabBarItem];
    tbi.image = [UIImage imageNamed:@"icon-tabbar-settings"];
    tbi.title = NSLocalizedString(@"Settings", @"settings tab bar title");
    return tbi;
}

- (BOOL)displayRightBarButtonsForChildViewController:(UIViewController *)childViewController; {
    return NO;
}

@end
