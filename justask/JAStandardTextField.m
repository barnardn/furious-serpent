//
//  JAStandardTextField.m
//  justask
//
//  Created by Norm Barnard on 2/16/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JAAppearanceManager.h"
#import "JAStandardTextField.h"

@implementation JAStandardTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    [self setTextColor:[[JAAppearanceManager appearanceManager] bodyTextColor]];
    [self setFont:[[JAAppearanceManager appearanceManager] userInputFont]];
    return self;
}

@end
