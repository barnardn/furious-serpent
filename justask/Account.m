#import <MECoreDataKit/MECoreDataKit.h>
#import <MEFoundation/MEFoundation.h>

#import "Account.h"
#import "JAAppController.h"

#import "NSDate+PostgresTimestampExtensions.h"

@interface Account ()

@end


@implementation Account

#pragma mark - core data overrides

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    self.createdAt = [NSDate date];
}


+ (instancetype)currentAccount;
{
    static Account *staticInstance;
    
    if(!staticInstance) {
        MELogObject(@"!!fetching account")
        staticInstance = [self accountInContext:[JAAppController appController].mainManagedObjectContext];
    }
    
    return staticInstance;
}

+ (instancetype)accountInContext:(NSManagedObjectContext *)context;
{
    NSError *error;
    NSArray *results = [context ME_fetchEntityNamed:[Account entityName] limit:1 predicate:nil sortDescriptors:nil error:&error];
    MEAssert(error == nil, @"Unable to fetch account information: %@", error);
    return results.firstObject;
}

+ (instancetype)createNewAccountInContext:(NSManagedObjectContext *)context;
{
    return [Account insertInManagedObjectContext:context];
}

+ (instancetype)createAccountFromJson:(NSDictionary *)json context:(NSManagedObjectContext *)context;
{
    Account *account = [self createNewAccountInContext:context];
    account.identity = json[AccountAttributes.identity];
    account.email = json[AccountAttributes.email];
    NSNumber *timestamp = json[AccountAttributes.createdAt];
    account.createdAt = [NSDate dateWithTimeIntervalSince1970:timestamp.unsignedLongLongValue];
    [context ME_saveRecursively:NULL];
    return account;
}

@end
