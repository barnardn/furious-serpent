//
//  JAVoterViewModel.m
//  justask
//
//  Created by Norm Barnard on 8/10/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <MECoreDataKit/MECoreDataKit.h>

#import "JustAsk.h"
#import "JAAppController.h"
#import "JANetworkClient.h"
#import "JADataModel.h"
#import "JAProfileViewModel.h"

static NSString * const kAPIPath = @"/api/v1/profile";
static NSString * const kGravatarURLFormat = @"http://www.gravatar.com/avatar/%@?s=%@";
static NSString * const kAvatarFilename = @"avatar.png";

@interface JAProfileViewModel() 

@property (strong, nonatomic, readwrite) RACCommand *profileImageCommand;

@end

@implementation JAProfileViewModel

- (instancetype)initWithProfile:(Profile *)profile;
{
    NSParameterAssert(profile);
    
    self = [super init];
    if (!self) return nil;
    
    _profile = profile;
    
    RACChannelTo(self, firstName) = RACChannelTo(self.profile, firstName);
    RACChannelTo(self, lastName) = RACChannelTo(self.profile, lastName);
    RACChannelTo(self, nickName) = RACChannelTo(self.profile, nickName);
    RACChannelTo(self, approved) = RACChannelTo(self.profile, approvedValue);
    RACChannelTo(self, adhoc, @NO) = RACChannelTo(self.profile, adhocValue, @NO);
    RACChannelTo(self, updatedAt) = RACChannelTo(self.profile, updatedAt);
    RACChannelTo(self, city) = RACChannelTo(self.profile, city);
    RACChannelTo(self, country) = RACChannelTo(self.profile, country);
    
    RAC(self, avatarURL) = [[RACObserve(self.profile, avatarUrl) ignore:nil] map:^id(NSString *value) {
        return [NSURL URLWithString:value];
    }];
    
    RAC(self, fullName) = [RACSignal combineLatest:@[RACObserve(self.profile, firstName), RACObserve(self.profile, lastName)] reduce:^id(NSString *fn, NSString *ln){
        return [NSString stringWithFormat:@"%@ %@", fn, ln];
    }];
    
    RAC(self, profileImageURL) = [[[RACObserve(self, profile) ignore:nil] distinctUntilChanged] map:^id(Profile *profile) {
        return [[JAAppController appController] urlForResourceInApplicationSupport:kAvatarFilename];
    }];
    
    RAC(self, gravatarURL) = [[[RACObserve(self, profile) ignore:nil] distinctUntilChanged] map:^id(Profile *profile) {
        NSString *gravatarHash = [[profile.account.email lowercaseString] ME_MD5String];
        CGFloat imageSize = 100 * [UIScreen mainScreen].scale;
        return  [NSURL URLWithString:[NSString stringWithFormat:kGravatarURLFormat, gravatarHash, [@(imageSize) stringValue]]];
    }];
    
    self.profileImageCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            [subscriber sendNext:self];
            [subscriber sendCompleted];
            return nil;
        }];
    }];
    
    return self;
}

- (instancetype)initWithAccount:(Account *)account
{
    NSParameterAssert(account);
    Profile *profile = [Profile insertInManagedObjectContext:account.managedObjectContext];
    account.profile = profile;
    return [self initWithProfile:profile];
}

- (RACSignal *)profileImage {
    return [[RACObserve(self, profileImageURL) mapReplace:self]
        flattenMap:^RACStream *(JAProfileViewModel *viewModel) {
            if ([viewModel.profileImageURL checkResourceIsReachableAndReturnError:NULL]) {
                NSURL *localURL = viewModel.profileImageURL;
                RACTuple *tuple = [RACTuple tupleWithObjectsFromArray:@[localURL, @0, @0]];
                return [RACSignal return:tuple];
            } else {
                if (viewModel.avatarURL)
                    return [[JANetworkClient client] downloadResourceFromURL:viewModel.avatarURL toLocalURL:viewModel.profileImageURL headers:nil];
                else
                    return [self _defaultAvatar];
            }
        }];
}

- (RACCommand *)saveProfileCommand {
    
    RACSignal *canSaveProfile = [RACSignal
                                 combineLatest:@[RACObserve(self, firstName), RACObserve(self, lastName)]
                                 reduce:^id(NSString *fname, NSString *lname) {
                                     return @((fname.length > 0) && (lname.length > 0));
                                 }];
    
    @weakify(self);
    return [[RACCommand alloc] initWithEnabled:canSaveProfile signalBlock:^RACSignal *(id _) {
        @strongify(self);
        
        RACSignal *requestSignal;
        if (self.profile.identity.length == 0) {
            NSString *path = kAPIPath;
            requestSignal = [[JANetworkClient client] postRequestAtPath:path parameters:[self.profile jsonSerialization]];
        } else {
            NSString *path = [NSString stringWithFormat:@"%@/%@", kAPIPath, self.profile.identity];
            requestSignal = [[JANetworkClient client] putRequestAtPath:path parameters:[self.profile jsonSerialization]];
        }
        
        return [requestSignal doNext:^(NSDictionary *json) {
            NSString *identity = json[ProfileAttributes.identity];
            self.profile.identity = identity;
            [self.profile.managedObjectContext ME_saveRecursively:NULL];
        }];
        
    }];
    
}

@dynamic avatarImageLength;
- (CGFloat)avatarImageLength {
    return 100.0f * [UIScreen mainScreen].scale;
}

- (RACSignal *)_defaultAvatar {
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        NSString *filename = ([UIScreen mainScreen].scale > 1.0) ? @"default-avatar@2x.png" : @"default-avatar.png";
        NSURL *url = [[JAAppController appController] urlForResourceInMainBundle:filename];
        [subscriber sendNext:[RACTuple tupleWithObjectsFromArray:@[url, @0, @0]]];
        [subscriber sendCompleted];
        return nil;
        
    }];
    
}


#pragma mark - class methods

+ (NSString *)avatarFilename {
    return kAvatarFilename;
}

@end
