// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SecurityQuestion.m instead.

#import "_SecurityQuestion.h"

const struct SecurityQuestionAttributes SecurityQuestionAttributes = {
	.answer = @"answer",
	.identity = @"identity",
	.question = @"question",
	.userDefined = @"userDefined",
};

@implementation SecurityQuestionID
@end

@implementation _SecurityQuestion

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"SecurityQuestion" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"SecurityQuestion";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"SecurityQuestion" inManagedObjectContext:moc_];
}

- (SecurityQuestionID*)objectID {
	return (SecurityQuestionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"userDefinedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"userDefined"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic answer;

@dynamic identity;

@dynamic question;

@dynamic userDefined;

- (BOOL)userDefinedValue {
	NSNumber *result = [self userDefined];
	return [result boolValue];
}

- (void)setUserDefinedValue:(BOOL)value_ {
	[self setUserDefined:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveUserDefinedValue {
	NSNumber *result = [self primitiveUserDefined];
	return [result boolValue];
}

- (void)setPrimitiveUserDefinedValue:(BOOL)value_ {
	[self setPrimitiveUserDefined:[NSNumber numberWithBool:value_]];
}

@end

