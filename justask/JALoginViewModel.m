//
//  JALoginViewModel.m
//  justask
//
//  Created by Norm Barnard on 9/3/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import <MECoreDataKit/MECoreDataKit.h>
#import <SVProgressHUD/SVProgressHUD.h>

#import "Account.h"
#import "JustAsk.h"
#import "JAAppController.h"
#import "JALoginViewModel.h"
#import "JANetworkClient.h"
#import "NSDate+PostgresTimestampExtensions.h"
#import "NSString+InputValidation.h"

static NSString * const kLoginAPIPath = @"/api/v1/auth";

@interface JALoginViewModel()

@property (strong, nonatomic, readwrite) RACCommand *loginCommand;

@end

@implementation JALoginViewModel


- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    @weakify(self);
    _loginCommand = [[RACCommand alloc]
                     initWithEnabled:[RACSignal combineLatest:@[RACObserve(self, email), RACObserve(self, password)] reduce:^id (NSString *email, NSString *password) {
        return @([email isValidRFC2822EmailAddress] && [password isValidPasswordOrViolatingRules:NULL]);
        
    }] signalBlock:^RACSignal *(id _) {
        
        return
        [[[[[JANetworkClient client] postRequestAtPath:kLoginAPIPath parameters:@{@"email" : self.email, @"password" : [self.password ME_SHA1String]}]
        initially:^{
            [SVProgressHUD show];
        }]
        finally:^{
            [SVProgressHUD dismiss];
        }] doNext:^(NSDictionary *json) {

            NSDictionary *session = json[@"session"];
            NSDictionary *account = json[@"account"];
            
            @strongify(self);
            [[JANetworkClient client] setSessionToken:session[@"sessionToken"]];
            [self _createOrUpdateAccountWithInfo:account plainTextPassword:self.password];
            
        }];
    }];
    
    return self;
}

- (void)_createOrUpdateAccountWithInfo:(NSDictionary *)accountInfo plainTextPassword:(NSString *)password
{
    Account *account = [Account currentAccount];
    if (!account) {
        account = [Account createAccountFromJson:accountInfo context:[JAAppController appController].mainManagedObjectContext];
        account.password = password;
        [account.managedObjectContext ME_saveRecursively:NULL];
    }
}



@end
