//
//  DataModel.h
//  justask
//
//  Created by Norm Barnard on 8/10/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//


#import "Account.h"
#import "Ballot.h"
#import "Invitation.h"
#import "Option.h"
#import "Profile.h"
#import "Proposition.h"

