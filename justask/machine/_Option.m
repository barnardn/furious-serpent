// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Option.m instead.

#import "_Option.h"

const struct OptionAttributes OptionAttributes = {
	.identity = @"identity",
	.summary = @"summary",
	.writeInText = @"writeInText",
};

const struct OptionRelationships OptionRelationships = {
	.ballots = @"ballots",
	.proposition = @"proposition",
};

@implementation OptionID
@end

@implementation _Option

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Option" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Option";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Option" inManagedObjectContext:moc_];
}

- (OptionID*)objectID {
	return (OptionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic identity;

@dynamic summary;

@dynamic writeInText;

@dynamic ballots;

- (NSMutableSet*)ballotsSet {
	[self willAccessValueForKey:@"ballots"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"ballots"];

	[self didAccessValueForKey:@"ballots"];
	return result;
}

@dynamic proposition;

@end

