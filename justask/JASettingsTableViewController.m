//
//  JASettingsTableViewController.m
//  justask
//
//  Created by Norm Barnard on 12/24/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#import "JAAppearanceManager.h"
#import "JASettingsProfileViewController.h"
#import "JALegalTermsTableViewController.h"
#import "JASettingsTableViewController.h"

typedef NS_ENUM(NSUInteger, SettingsSections) {
    SettingsSectionsProfile,
    SettingsSectionsLegal,
    SettingsSectionsEndMarker           // used for its value only
};

@implementation JASettingsTableViewController

- (instancetype)init
{
    self = [self initWithStyle:UITableViewStyleGrouped];
    if (!self) return nil;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
    self.tableView.backgroundColor = [JAAppearanceManager appearanceManager].groupedTableViewBackgroundColor;
}

- (NSString *)title {
    return NSLocalizedString(@"Settings", @"settings tab view controller title");
}

#pragma mark - tableview datasource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return SettingsSectionsEndMarker;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class]) forIndexPath:indexPath];
    
    cell.backgroundColor = [JAAppearanceManager appearanceManager].whiteColor;
    cell.textLabel.font = [JAAppearanceManager appearanceManager].titleFont;
    cell.textLabel.textColor = [JAAppearanceManager appearanceManager].bodyTextColor;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    NSString *title;
    if (indexPath.section == SettingsSectionsProfile)
        title = NSLocalizedString(@"Profile", @"settings view profile cell title");
    else
        title = NSLocalizedString(@"Legal", @"settings view legal cell title");
    
    cell.textLabel.text = title;
    return cell;
}

#pragma mark - tableview delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *viewController;
    if (indexPath.section == SettingsSectionsProfile)
        viewController = [[JASettingsProfileViewController alloc] init];
    else
        viewController = [[JALegalTermsTableViewController alloc] init];
    
    [self.navigationController pushViewController:viewController animated:YES];
}



@end
