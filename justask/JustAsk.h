//
//  JustAsk.h
//  justask
//
//  Created by Norm Barnard on 9/1/14.
//  Copyright (c) 2014 Clamdango. All rights reserved.
//

#ifndef justask_JAConstants_h
#define justask_JAConstants_h

#import <libextobjc/EXTScope.h>
#import <Foundation/Foundation.h>
#import <ReactiveCocoa/RACEXTScope.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <MEFoundation/MEFoundation.h>
#import <MEReactiveFoundation/MEReactiveFoundation.h>
#import <MECoreDataKit/MECoreDataKit.h>

static const CGFloat JASubviewMargin = 20.0f;
static const CGFloat JASubviewSpacing = 8.0f;

extern NSString * const JustAskApplicationErrorDomain; //= @"com.justask.app-error";
extern NSString * const JustAskApplicationNetworkErrorKey; //= @"JustAskApplicationNetworkErrorKey";


#endif
