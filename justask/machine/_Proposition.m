// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Proposition.m instead.

#import "_Proposition.h"

const struct PropositionAttributes PropositionAttributes = {
	.allowVotersToChangeBallot = @"allowVotersToChangeBallot",
	.allowWriteInOptions = @"allowWriteInOptions",
	.expiresAt = @"expiresAt",
	.identity = @"identity",
	.maxAllowedSelections = @"maxAllowedSelections",
	.summary = @"summary",
	.template = @"template",
};

const struct PropositionRelationships PropositionRelationships = {
	.ballots = @"ballots",
	.options = @"options",
};

@implementation PropositionID
@end

@implementation _Proposition

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Proposition" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Proposition";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Proposition" inManagedObjectContext:moc_];
}

- (PropositionID*)objectID {
	return (PropositionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"allowVotersToChangeBallotValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"allowVotersToChangeBallot"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"allowWriteInOptionsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"allowWriteInOptions"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"maxAllowedSelectionsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"maxAllowedSelections"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"templateValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"template"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic allowVotersToChangeBallot;

- (BOOL)allowVotersToChangeBallotValue {
	NSNumber *result = [self allowVotersToChangeBallot];
	return [result boolValue];
}

- (void)setAllowVotersToChangeBallotValue:(BOOL)value_ {
	[self setAllowVotersToChangeBallot:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveAllowVotersToChangeBallotValue {
	NSNumber *result = [self primitiveAllowVotersToChangeBallot];
	return [result boolValue];
}

- (void)setPrimitiveAllowVotersToChangeBallotValue:(BOOL)value_ {
	[self setPrimitiveAllowVotersToChangeBallot:[NSNumber numberWithBool:value_]];
}

@dynamic allowWriteInOptions;

- (BOOL)allowWriteInOptionsValue {
	NSNumber *result = [self allowWriteInOptions];
	return [result boolValue];
}

- (void)setAllowWriteInOptionsValue:(BOOL)value_ {
	[self setAllowWriteInOptions:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveAllowWriteInOptionsValue {
	NSNumber *result = [self primitiveAllowWriteInOptions];
	return [result boolValue];
}

- (void)setPrimitiveAllowWriteInOptionsValue:(BOOL)value_ {
	[self setPrimitiveAllowWriteInOptions:[NSNumber numberWithBool:value_]];
}

@dynamic expiresAt;

@dynamic identity;

@dynamic maxAllowedSelections;

- (int16_t)maxAllowedSelectionsValue {
	NSNumber *result = [self maxAllowedSelections];
	return [result shortValue];
}

- (void)setMaxAllowedSelectionsValue:(int16_t)value_ {
	[self setMaxAllowedSelections:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveMaxAllowedSelectionsValue {
	NSNumber *result = [self primitiveMaxAllowedSelections];
	return [result shortValue];
}

- (void)setPrimitiveMaxAllowedSelectionsValue:(int16_t)value_ {
	[self setPrimitiveMaxAllowedSelections:[NSNumber numberWithShort:value_]];
}

@dynamic summary;

@dynamic template;

- (BOOL)templateValue {
	NSNumber *result = [self template];
	return [result boolValue];
}

- (void)setTemplateValue:(BOOL)value_ {
	[self setTemplate:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveTemplateValue {
	NSNumber *result = [self primitiveTemplate];
	return [result boolValue];
}

- (void)setPrimitiveTemplateValue:(BOOL)value_ {
	[self setPrimitiveTemplate:[NSNumber numberWithBool:value_]];
}

@dynamic ballots;

- (NSMutableSet*)ballotsSet {
	[self willAccessValueForKey:@"ballots"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"ballots"];

	[self didAccessValueForKey:@"ballots"];
	return result;
}

@dynamic options;

- (NSMutableSet*)optionsSet {
	[self willAccessValueForKey:@"options"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"options"];

	[self didAccessValueForKey:@"options"];
	return result;
}

@end

